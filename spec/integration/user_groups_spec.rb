describe "user groups" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
  end

  # USER GROUP INDEX
  describe "GET /user_groups" do
    let!(:user_groups) { FactoryBot.create_list(:user_group, rand(2..5)) }
    before { authorize(:list, UserGroup) }
    it "should display user groups" do
      get "/user_groups"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(user_groups.sample.name)
    end
    context "when user is not authorized to list user groups" do
      before { unauthorize(:list, UserGroup) }
      it "should not display user groups" do
        get "/user_groups"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(user_groups.sample.name)
        expect(response.body).not_to include(user_groups.sample.id)
      end
    end
  end

  # SHOW USER GROUP
  describe "GET /user_groups/:id" do
    let(:user_group) { FactoryBot.create(:user_group) }
    before { authorize :read, user_group }
    it "should show user group" do
      get "/user_groups/#{user_group.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read user_group" do
      before { unauthorize :read, user_group }
      it "should not show user group" do
        get "/user_groups/#{user_group.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # ADD USER
  describe "POST on /user_groups/:id/users" do
    let!(:user_group) { FactoryBot.create(:user_group) }
    let!(:new_user) { FactoryBot.create(:user) }
    before { authorize :update, user_group }
    it "should add user" do
      post "/user_groups/#{user_group.id}/users", params: {
        user_id: new_user.id
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:add_user)
      user_group.reload
      expect(user_group.users).to include(new_user)
    end
    context "when user is already in group" do
      let!(:new_user) { FactoryBot.create(:user, groups: [user_group]) }
      it "should respond not modified" do
        post "/user_groups/#{user_group.id}/users", params: {
          user_id: new_user.id
        }
        expect(response).to have_http_status(:not_modified)
      end
    end
    context "when user is not authorized to update user group" do
      before { unauthorize :update, user_group }
      it "should not add user" do
        post "/user_groups/#{user_group.id}/users", params: {
          user_id: new_user.id
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        user_group.reload
        expect(user_group.users).not_to include(new_user)
      end
    end
  end

  # REMOVE USER
  describe "DELETE /user_groups/:id/users/:user_id" do
    let!(:new_user1) { FactoryBot.create(:user) }
    let!(:new_user2) { FactoryBot.create(:user) }
    let!(:user_group) { FactoryBot.create(:user_group, users: [new_user1, new_user2]) }
    before { authorize :update, user_group }
    it "should remove user" do
      delete "/user_groups/#{user_group.id}/users/#{new_user1.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:remove_user)
      user_group.reload
      expect(user_group.users).to contain_exactly(new_user2)
    end
    context "when user is not in group" do
      let!(:user_group) { FactoryBot.create(:user_group, users: [new_user2]) }
      it "should respond not modified" do
        delete "/user_groups/#{user_group.id}/users/#{new_user1.id}"
        expect(response).to have_http_status(:not_modified)
      end
    end
    context "when user is not authorized to update user_group" do
      before { unauthorize :update, user_group }
      it "should not remove user" do
        delete "/user_groups/#{user_group.id}/users/#{new_user1.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        user_group.reload
        expect(user_group.users).to contain_exactly(new_user1, new_user2)
      end
    end
  end
end
