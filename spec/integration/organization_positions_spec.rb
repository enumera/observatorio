describe "organization positions" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # POSITIONS INDEX
  describe "GET /organization_positions" do
    let!(:suffix1) { "Facility" }
    let!(:suffix2) { "Nature" }

    let!(:organization_position1) { FactoryBot.create(:organization_position, name: "Organization Position A #{suffix1}") }
    let!(:organization_position2) { FactoryBot.create(:organization_position, name: "Organization Position B #{suffix1}") }
    let!(:organization_position3) { FactoryBot.create(:organization_position, name: "Organization Position C #{suffix2}") }
    let!(:organization_position4) { FactoryBot.create(:organization_position, name: "Organization Position D #{suffix2}") }
    let!(:organization_position5) { FactoryBot.create(:organization_position, name: "Organization Position E #{suffix2}") }
    let!(:organization_position6) { FactoryBot.create(:organization_position, name: "Organization Position F #{suffix2}") }
    let!(:organization_position7) { FactoryBot.create(:organization_position, name: "Organization Position G #{suffix2}") }
    let!(:organization_position8) { FactoryBot.create(:organization_position, name: "Organization Position H #{suffix2}") }
    let!(:organization_position9) { FactoryBot.create(:organization_position, name: "Organization Position I #{suffix2}") }
    let!(:organization_position10) { FactoryBot.create(:organization_position, name: "Organization Position J #{suffix2}") }
    let!(:organization_position11) { FactoryBot.create(:organization_position, name: "Organization Position K #{suffix2}") }
    let!(:organization_position12) { FactoryBot.create(:organization_position, name: "Organization Position L #{suffix2}") }

    before { authorize(:list, OrganizationPosition) }
    it "should list only 8 organization positions per page" do
      get "/organization_positions"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_position1.name)
      expect(response.body).to include(organization_position2.name)
      expect(response.body).to include(organization_position3.name)
      expect(response.body).to include(organization_position4.name)
      expect(response.body).to include(organization_position5.name)
      expect(response.body).to include(organization_position6.name)
      expect(response.body).to include(organization_position7.name)
      expect(response.body).to include(organization_position8.name)
      expect(response.body).not_to include(organization_position9.name)
      expect(response.body).not_to include(organization_position10.name)
      expect(response.body).not_to include(organization_position11.name)
      expect(response.body).not_to include(organization_position12.name)
    end
    it "should limit search results to 8 organization positions per page" do
      get "/organization_positions", params: { search: suffix2 }
      expect(response.body).to include(organization_position3.name)
      expect(response.body).to include(organization_position4.name)
      expect(response.body).to include(organization_position5.name)
      expect(response.body).to include(organization_position6.name)
      expect(response.body).to include(organization_position7.name)
      expect(response.body).to include(organization_position8.name)
      expect(response.body).to include(organization_position9.name)
      expect(response.body).to include(organization_position10.name)
      expect(response.body).not_to include(organization_position11.name)
      expect(response.body).not_to include(organization_position12.name)
    end
    it "should only return 2 organization positions" do
      get "/organization_positions", params: { search: suffix1 }
      expect(response.body).to include(organization_position1.name)
      expect(response.body).to include(organization_position2.name)
      expect(response.body).not_to include(organization_position3.name)
      expect(response.body).not_to include(organization_position4.name)
      expect(response.body).not_to include(organization_position5.name)
      expect(response.body).not_to include(organization_position6.name)
      expect(response.body).not_to include(organization_position7.name)
      expect(response.body).not_to include(organization_position8.name)
      expect(response.body).not_to include(organization_position9.name)
      expect(response.body).not_to include(organization_position10.name)
      expect(response.body).not_to include(organization_position11.name)
      expect(response.body).not_to include(organization_position12.name)
    end
    context "when user is not authorized to list positions" do
      before { unauthorize(:list, OrganizationPosition) }
      it "should not list positions" do
        get "/organization_positions", params: { query: organization_position1.name }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(organization_position1.name)
        expect(response.body).not_to include(organization_position1.id)
      end
    end
  end

  # NEW POSITION FORM
  describe "GET /organization_positions/new" do
    before { authorize :create, OrganizationPosition }
    it "should render new position form" do
      get "/organization_positions/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create positions" do
      before { unauthorize :create, OrganizationPosition }
      it "should not render new position form" do
        get "/organization_positions/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE POSITION
  describe "POST /organization_positions" do
    let(:name) { random_string }
    before { authorize(:create, OrganizationPosition) }
    it "should create organization position" do
      expect {
        post "/organization_positions", params: {
          organization_position: {
            name: name
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(OrganizationPosition, :count).from(0).to(1)
      created_position = OrganizationPosition.first
      expect(created_position.name).to eq(name)
    end
    context "when validation fails" do
      it "should not create position" do
        expect {
          post "/organization_positions", params: {
            organization_position: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(OrganizationPosition, :count)
      end
    end
    context "when user is not authorized to create organization positions" do
      before { unauthorize(:create, OrganizationPosition) }
      it "should not create position" do
        expect {
          post "/organization_positions", params: {
            organization_position: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationPosition, :count)
      end
    end
  end

  # SHOW POSITION
  describe "GET /organization_positions/:id" do
    let(:organization_position) { FactoryBot.create(:organization_position) }
    let!(:organization_member) { FactoryBot.create_list(:organization_member, rand(5..10), position: organization_position) }
    before { authorize(:read, organization_position) }
    it "should show position" do
      get "/organization_positions/#{organization_position.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the position" do
      before { unauthorize(:read, organization_position) }
      it "should not show organization position" do
        get "/organization_positions/#{organization_position.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # SHOW ORGANIZATION POSITION'S HISTORY
  describe "GET /organization_positions/:id/history" do
    let!(:organization_position) { FactoryBot.create(:organization_position) }
    before { authorize :read, organization_position }
    it "should show organization_positions's changes history" do
      get "/organization_positions/#{organization_position.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read organization_position" do
      before { unauthorize :read, organization_position }
      it "should not show organization_positions's changes history" do
        get "/organization_positions/#{organization_position.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT POSITION FORM
  describe "GET /organization_positions/:id/edit" do
    let(:position) { FactoryBot.create(:organization_position) }
    before { authorize :update, position }
    it "should render edit position form" do
      get "/organization_positions/#{position.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit position" do
      before { unauthorize :update, position }
      it "should not render edit position form" do
        get "/organization_positions/#{position.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE POSITION
  describe "PUT /organization_positions/:id" do
    let!(:organization_position) { FactoryBot.create(:organization_position) }
    let(:name) { random_string }
    before { authorize(:update, organization_position) }
    it "should update organization position" do
      put "/organization_positions/#{organization_position.id}", params: {
        organization_position: {
          name: name
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      organization_position.reload
      expect(organization_position.name).to eq(name)
    end
    context "when validation fails" do
      it "should not update organization position" do
        expect {
          put "/organization_positions/#{organization_position.id}", params: {
            organization_position: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { organization_position.reload.name }
      end
    end
    context "when user is not authorized to update organization position" do
      before { unauthorize(:update, organization_position) }
      it "should not update organization position" do
        expect {
          put "/organization_positions/#{organization_position.id}", params: {
            organization_position: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { organization_position.reload.name }
      end
    end
  end

  # DESTROY POSITION
  describe "DELETE /organization_positions/:id" do
    let!(:organization_position) { FactoryBot.create(:organization_position) }
    before { authorize(:delete, organization_position) }
    it "should delete organization position" do
      expect {
        delete "/organization_positions/#{organization_position.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(OrganizationPosition, :count).from(1).to(0)
    end
    context "when dependent organization member" do
      before { FactoryBot.create(:organization_member, position: organization_position) }
      it "should not delete organization position" do
        expect {
          delete "/organization_positions/#{organization_position.id}"
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:error)
        }.not_to change(OrganizationPosition, :count)
      end
    end
    context "when user is not authorized to delete the position" do
      before { unauthorize(:delete, organization_position) }
      it "should not delete organization position" do
        expect {
          delete "/organization_positions/#{organization_position.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationPosition, :count)
      end
    end
  end
end
