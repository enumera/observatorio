describe "organizations owners" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # ORGANIZATION MEMBERS INDEX
  describe "GET /organization_owners" do
    let!(:owner) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_owner1) do
      FactoryBot.create(:organization_owner, owner: owner, organization: organization)
    end
    let!(:organization_owner2) do
      FactoryBot.create(:organization_owner, owner: owner)
    end
    let!(:organization_owner3) do
      FactoryBot.create(:organization_owner, organization: organization)
    end
    before do
      authorize :read, owner
      authorize :read, organization
    end
    it "should list organizations of a owner" do
      get "/organization_owners", params: {
        organization_owner: { owner_id: owner.id, owner_type: owner.class }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_owner1.id)
      expect(response.body).to include(organization_owner2.id)
      expect(response.body).not_to include(organization_owner3.id)
    end
    it "should list owners of a organization" do
      get "/organization_owners", params: {
        organization_owner: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_owner1.id)
      expect(response.body).not_to include(organization_owner2.id)
      expect(response.body).to include(organization_owner3.id)
    end
    context "when neither owner nor organization provided" do
      it "should respond bad request" do
        get "/organization_owners"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read owner" do
      before { unauthorize :read, owner }
      it "should respond forbidden" do
        get "/organization_owners", params: {
          organization_owner: { owner_id: owner.id, owner_type: owner.class }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should respond forbidden" do
        get "/organization_owners", params: {
          organization_owner: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW ORGANIZATION OWNER FORM
  describe "GET /organization_owners/new" do
    let(:owner) { FactoryBot.create(:person) }
    let(:organization) { FactoryBot.create(:organization) }
    before do
      authorize :update, owner
      authorize :update, organization
    end
    it "should render new organization owner form for a owner" do
      get "/organization_owners/new", params: {
        organization_owner: { owner_id: owner.id, owner_type: owner.class }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    it "should render new organization owner form for a organization" do
      get "/organization_owners/new", params: {
        organization_owner: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when neither owner nor organization is given" do
      it "should respond bad request" do
        get "/organization_owners/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should respond forbidden" do
        get "/organization_owners/new", params: {
          organization_owner: { owner_id: owner.id, owner_type: owner.class }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should respond forbidden" do
        get "/organization_owners/new", params: {
          organization_owner: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE ORGANIZATION OWNER
  describe "POST /organization_owners" do
    let(:owner) { FactoryBot.create(:person) }
    let(:organization) { FactoryBot.create(:organization) }
    let(:portion_percentage) { rand(0..80) }
    let(:from_date) { random_date }
    let(:to_date) { from_date + 1.day }
    let(:approximate_dates) { [true, false].sample }
    before do
      authorize :update, owner
      authorize :update, organization
    end
    it "should create organization_owner" do
      expect {
        post "/organization_owners", params: {
          organization_owner: {
            owner_id: owner.id,
            owner_type: owner.class,
            organization_id: organization.id,
            portion_percentage: portion_percentage,
            from_date: from_date,
            to_date: to_date,
            approximate_dates: approximate_dates,
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(OrganizationOwner, :count).from(0).to(1)
      created = OrganizationOwner.last
      expect(created.owner).to eq(owner)
      expect(created.organization).to eq(organization)
      expect(created.portion * 100).to be_within(0.1).of(portion_percentage)
      expect(created.from_date).to eq(from_date)
      expect(created.to_date).to eq(to_date)
      expect(created.approximate_dates).to eq(approximate_dates)
    end
    context "when validation fails" do
      it "should not create organization_owner" do
        expect {
          post "/organization_owners", params: {
            organization_owner: {
              owner_id: owner.id,
              owner_type: owner.class,
              organization_id: organization.id
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(OrganizationOwner, :count)
      end
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not create organization_owner" do
        expect {
          post "/organization_owners", params: {
            organization_owner: {
              owner_id: owner.id,
              owner_type: owner.class,
              organization_id: organization.id,
              portion_percentage: portion_percentage
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationOwner, :count)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not create organization_owner" do
        expect {
          post "/organization_owners", params: {
            organization_owner: {
              owner_id: owner.id,
              owner_type: owner.class,
              organization_id: organization.id,
              portion_percentage: portion_percentage
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationOwner, :count)
      end
    end
  end

  # SHOW ORGANIZATION OWNER
  describe "GET /organization_owners/:id" do
    let!(:owner) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_owner) do
      FactoryBot.create(:organization_owner, organization: organization, owner: owner)
    end
    before do
      authorize :read, owner
      authorize :read, organization
    end
    it "should render organization_owner" do
      get "/organization_owners/#{organization_owner.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read owner" do
      before { unauthorize :read, owner }
      it "should not render organizations_owner" do
        get "/organization_owners/#{organization_owner.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should not render organizations_owner" do
        get "/organization_owners/#{organization_owner.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT ORGANIZATION OWNER FORM
  describe "GET /organization_owners/:id/edit" do
    let!(:owner) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_owner) do
      FactoryBot.create(:organization_owner, organization: organization, owner: owner)
    end
    before do
      authorize :update, owner
      authorize :update, organization
    end
    it "should render edit organization_owner form" do
      get "/organization_owners/#{organization_owner.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not render edit organization_owner form" do
        get "/organization_owners/#{organization_owner.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not render edit organization_owner form" do
        get "/organization_owners/#{organization_owner.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ORGANIZATION OWNER
  describe "PUT /organization_owners/:id" do
    let!(:owner) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_owner) do
      FactoryBot.create(:organization_owner, organization: organization, owner: owner)
    end
    let(:new_portion_percentage) { rand(0..80) }
    before do
      authorize :update, owner
      authorize :update, organization
    end
    it "should update organization_owner" do
      put "/organization_owners/#{organization_owner.id}", params: {
        organization_owner: { portion_percentage: new_portion_percentage }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      organization_owner.reload
      expect(organization_owner.portion * 100).to be_within(0.1).of(new_portion_percentage)
    end
    context "when validation fails" do
      it "should not update organization_owner" do
        expect {
          put "/organization_owners/#{organization_owner.id}", params: {
            organization_owner: { portion_percentage: "" }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(organization_owner.reload, :portion)
      end
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not update organization_owner" do
        put "/organization_owners/#{organization_owner.id}", params: {
          organization_owner: { portion_percentage: new_portion_percentage }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        organization_owner.reload
        expect(organization_owner.portion * 100).not_to be_within(0.1).of(new_portion_percentage)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not update organization_owner" do
        put "/organization_owners/#{organization_owner.id}", params: {
          organization_owner: { portion_percentage: new_portion_percentage }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        organization_owner.reload
        expect(organization_owner.portion * 100).not_to be_within(0.1).of(new_portion_percentage)
      end
    end
  end

  # DESTROY ORGANIZATION OWNER
  describe "DELETE /organization_owners/:id" do
    let!(:owner) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_owner) do
      FactoryBot.create(:organization_owner, organization: organization, owner: owner)
    end
    let(:new_portion_percentage) { rand(0..100) }
    before do
      authorize :update, owner
      authorize :update, organization
    end
    it "should delete organization_owner" do
      expect {
        delete "/organization_owners/#{organization_owner.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(OrganizationOwner, :count).from(1).to(0)
    end
    context "when user is not authorized to update owner" do
      before { unauthorize :update, owner }
      it "should not delete organization_owner" do
        expect {
          delete "/organization_owners/#{organization_owner.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationOwner, :count)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not delete organization_owner" do
        expect {
          delete "/organization_owners/#{organization_owner.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationOwner, :count)
      end
    end
  end
end
