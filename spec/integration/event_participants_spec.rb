describe "event participants" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # EVENT PARTICIPANT INDEX
  describe "GET /event_participants" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:event_participant1) do
      FactoryBot.create(:event_participant, event: event, participant: participant)
    end
    let!(:event_participant2) do
      FactoryBot.create(:event_participant, event: event)
    end
    let!(:event_participant3) do
      FactoryBot.create(:event_participant, participant: participant)
    end
    before do
      authorize :read, event
      authorize :read, participant
    end
    it "should list participants of an event" do
      get "/event_participants", params: {
        event_participant: { event_id: event.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(event_participant1.id)
      expect(response.body).to include(event_participant2.id)
      expect(response.body).not_to include(event_participant3.id)
    end
    it "should list events of a participant" do
      get "/event_participants", params: {
        event_participant: {
          participant_id: participant.id,
          participant_type: participant.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(event_participant1.id)
      expect(response.body).not_to include(event_participant2.id)
      expect(response.body).to include(event_participant3.id)
    end
    context "when user is not authorized to read event" do
      before { unauthorize :read, event }
      it "shoult not list participants" do
        get "/event_participants", params: {
          event_participant: { event_id: event.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read participant" do
      before { unauthorize :read, participant }
      it "shoult not list participants" do
        get "/event_participants", params: {
          event_participant: {
            participant_id: participant.id,
            participant_type: participant.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW EVENT PARTICIPANT FORM
  describe "GET /event_participants/new" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    before do
      authorize :update, event
      authorize :update, participant
    end
    it "should render new event_participant form for an event" do
      get "/event_participants/new", params: {
        event_participant: { event_id: event.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    it "should render new event_participant form for a participant" do
      get "/event_participants/new", params: {
        event_participant: {
          participant_id: participant.id ,
          participant_type: participant.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update event" do
      before { unauthorize :update, event }
      it "should not render new event_participant form" do
        get "/event_participants/new", params: {
          event_participant: { event_id: event.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update participant" do
      before { unauthorize :update, participant }
      it "should not render new event_participant form" do
        get "/event_participants/new", params: {
          event_participant: {
            participant_id: participant.id ,
            participant_type: participant.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EVENT PARTICIPANT
  describe "POST /event_participants" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:role) { FactoryBot.create(:event_role) }
    let!(:from_datetime) { random_datetime }
    let!(:to_datetime) { from_datetime + 1.day }
    let!(:approximate_dates) { [true, false].sample }
    let!(:location) { FactoryBot.create(:location) }
    before do
      authorize :update, event
      authorize :update, participant
    end
    it "should create event participant" do
      expect {
        post "/event_participants", params: {
          event_participant: {
            event_id: event.id,
            participant_id: participant.id,
            participant_type: participant.class,
            role_id: role.id,
            from_datetime: from_datetime,
            to_datetime: to_datetime,
            approximate_dates: approximate_dates,
            location_id: location.id
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(EventParticipant, :count).from(0).to(1)
      created = EventParticipant.first
      expect(created.role).to eq(role)
      expect(created.from_datetime).to be_within(0.01).of(from_datetime)
      expect(created.to_datetime).to be_within(0.01).of(to_datetime)
      expect(created.approximate_dates).to eq(approximate_dates)
      expect(created.location).to eq(location)
    end
    context "when validation fails" do
      it "should not create event participant" do
        expect {
          post "/event_participants", params: {
            event_participant: {
              event_id: event.id,
              participant_id: participant.id,
              participant_type: participant.class,
              from_datetime: to_datetime,
              to_datetime: from_datetime
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(EventParticipant, :count)
      end
    end
    context "when user is not authorized to update event" do
      before { unauthorize :update, event }
      it "should not create event_participant" do
        expect {
          post "/event_participants", params: {
            event_participant: {
              event_id: event.id,
              participant_id: participant.id,
              participant_type: participant.class,
              role_id: role.id,
              from_datetime: from_datetime,
              to_datetime: to_datetime,
              approximate_dates: approximate_dates
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventParticipant, :count)
      end
    end
    context "when user is not authorized to update participant" do
      before { unauthorize :update, participant }
      it "should not create event_participant" do
        expect {
          post "/event_participants", params: {
            event_participant: {
              event_id: event.id,
              participant_id: participant.id,
              participant_type: participant.class,
              role_id: role.id,
              from_datetime: from_datetime,
              to_datetime: to_datetime,
              approximate_dates: approximate_dates
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventParticipant, :count)
      end
    end
  end

  # SHOW EVENT PARTICIPANT
  describe "GET /event_participants/:id" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:event_participant) do
      FactoryBot.create(:event_participant, event: event, participant: participant)
    end
    before do
      authorize :read, event
      authorize :read, participant
    end
    it "should render event_participant" do
      get "/event_participants/#{event_participant.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not unauthorize to read event" do
      before { unauthorize :read, event }
      it "should not render event_participant" do
        get "/event_participants/#{event_participant.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not unauthorize to read participant" do
      before { unauthorize :read, participant }
      it "should not render event_participant" do
        get "/event_participants/#{event_participant.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EVENT PARTICIPANT FORM
  describe "GET /event_participants/:id/edit" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:event_participant) do
      FactoryBot.create(:event_participant, event: event, participant: participant)
    end
    before do
      authorize :update, event
      authorize :update, participant
    end
    it "should render edit form" do
      get "/event_participants/#{event_participant.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update event" do
      before { unauthorize :update, event }
      it "should not render edit form" do
        get "/event_participants/#{event_participant.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update participant" do
      before { unauthorize :update, participant }
      it "should not render edit form" do
        get "/event_participants/#{event_participant.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EVENT PARTICIPANT
  describe "PUT /event_participants/:id" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:new_role) { FactoryBot.create(:event_role) }
    let!(:new_from_datetime) { random_datetime }
    let!(:new_to_datetime) { new_from_datetime + 1.day }
    let!(:new_approximate_dates) { [true, false].sample }
    let!(:new_location) { FactoryBot.create(:location) }
    let!(:event_participant) do
      FactoryBot.create(:event_participant, event: event, participant: participant)
    end
    before do
      authorize :update, event
      authorize :update, participant
    end
    it "should update event_participant" do
      put "/event_participants/#{event_participant.id}", params: {
        event_participant: {
          role_id: new_role.id,
          from_datetime: new_from_datetime,
          to_datetime: new_to_datetime,
          approximate_dates: new_approximate_dates,
          location_id: new_location.id
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      event_participant.reload
      expect(event_participant.role).to eq(new_role)
      expect(event_participant.from_datetime).to be_within(0.01).of(new_from_datetime)
      expect(event_participant.to_datetime).to be_within(0.01).of(new_to_datetime)
      expect(event_participant.approximate_dates).to eq(new_approximate_dates)
      expect(event_participant.location).to eq(new_location)
    end
  end

  # DESTROY EVENT PARTICIPANT
  describe "DELETE /event_participants/:id" do
    let!(:participant) { FactoryBot.create(:person) }
    let!(:event) { FactoryBot.create(:event) }
    let!(:event_participant) do
      FactoryBot.create(:event_participant, event: event, participant: participant)
    end
    before do
      authorize :update, event
      authorize :update, participant
    end
    it "should delete event_participant" do
      expect {
        delete "/event_participants/#{event_participant.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(EventParticipant, :count).from(1).to(0)
    end
    context "when user is not authorized to update event" do
      before { unauthorize :update, event }
      it "should not delete event_participant" do
        expect {
          delete "/event_participants/#{event_participant.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventParticipant, :count)
      end
    end
    context "when user is not authorized to update participant" do
      before { unauthorize :update, participant }
      it "should not delete event_participant" do
        expect {
          delete "/event_participants/#{event_participant.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventParticipant, :count)
      end
    end
  end
end
