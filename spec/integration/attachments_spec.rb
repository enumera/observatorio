describe "attachments" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # INDEX ATTACHMENTS
  describe "GET /attachments" do
    let(:person) { FactoryBot.create(:person) }
    let!(:attachment1) { FactoryBot.create(:attachment, attachable: person) }
    let!(:attachment2) { FactoryBot.create(:attachment, attachable: person) }
    let!(:attachment3) { FactoryBot.create(:attachment) }
    before { authorize(:read, person) }
    it "should list attachments" do
      get "/attachments", params: {
        attachment: {
          attachable_id: person.id,
          attachable_type: Person
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(attachment1.label)
      expect(response.body).to include(attachment2.label)
      expect(response.body).not_to include(attachment3.label)
    end
    context "when attachable_id is missing" do
      it "should give error" do
        get "/attachments", params: {
          attachment: { attachable_type: Person }
        }
        expect(response).to have_http_status(:bad_request)
      end
    end
    context "when attachable_type is missing" do
      it "should give error" do
        get "/attachments", params: {
          attachment: { attachable_id: attachment1.attachable_id }
        }
        expect(response).to have_http_status(:bad_request)
      end
    end
    context "when user is not authorized to read attachments attachable" do
      before { unauthorize :read, attachment1.attachable }
      it "should not list attachments" do
        get "/attachments", params: {
          attachment: {
            attachable_id: attachment1.attachable_id,
            attachable_type: attachment1.attachable_type
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW ATTACHMENT FORM
  describe "GET /attachments/new" do
    let(:person) { FactoryBot.create(:person) }
    before { authorize :update, person }
    it "should render new attachment form" do
      get "/attachments/new", params: {
        attachment: {
          attachable_id: person.id,
          attachable_type: person.class.name
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update attachable" do
      before { unauthorize :update, person }
      it "should not render new attachment form" do
        get "/attachments/new", params: {
          attachment: {
            attachable_id: person.id,
            attachable_type: person.class.name
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE ATTACHMENT
  describe "POST /attachments" do
    let(:label) { random_string }
    let(:person) { FactoryBot.create(:person) }
    let(:file) { Rack::Test::UploadedFile.new(File.join(Rails.root, "spec", "files", "meinhof.jpg"), "image/jpeg") }
    let(:description) { random_string }
    before { authorize(:update, person) }
    it "should create attachment" do
      expect {
        post "/attachments", params: {
          attachment: {
            label: label,
            description: description,
            file_unencrypted: file,
            attachable_id: person.id,
            attachable_type: Person
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Attachment, :count).from(0).to(1)
      created_attachment = Attachment.first
      expect(created_attachment.label).to eq(label)
      expect(created_attachment.attachable).to eq(person)
      expect(created_attachment.description).to eq(description)
    end
    context "when validation fails" do
      it "should not create attachment" do
        expect {
          post "/attachments", params: {
            attachment: {
              label: "",
              attachable_id: person.id,
              attachable_type: Person
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Attachment, :count)
      end
    end
    context "when user is not authorized to update person" do
      before { unauthorize(:update, person) }
      it "should not create attachment" do
        expect {
          post "/attachments", params: {
            attachment: {
              label: label,
              file_unencrypted: file,
              attachable_id: person.id,
              attachable_type: Person
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Attachment, :count)
      end
    end
  end

  # SHOW ATTACHMENT
  describe "GET /attachments/:id" do
    let(:person) { FactoryBot.create(:person) }
    let(:attachment) { FactoryBot.create(:attachment, attachable: person) }
    before { authorize :read, attachment }
    it "should show attachment" do
      get "/attachments/#{attachment.id}", params: {
        origin: {
          id: person.id,
          controller: "people"
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    [:pdf, :image, :video, :audio].each do |type|
      context "when content is #{type}" do
        let(:attachment) { FactoryBot.create(:attachment, type, attachable: person) }
        it "should show attachment" do
          get "/attachments/#{attachment.id}", params: {
            origin: {
              id: person.id,
              controller: "people"
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:show)
        end
      end
    end
  end

  # SHOW ATTACHMENT'S HISTORY
  describe "GET /attachments/:id/history" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:attachment) { FactoryBot.create(:attachment, attachable: person) }
    before { authorize :read, person }
    it "should show attachments's changes history" do
      get "/attachments/#{attachment.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read person" do
      before { unauthorize :read, person }
      it "should not show attachments's changes history" do
        get "/attachments/#{attachment.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT ATTACHMENT FORM
  describe "GET /attachments/:id/edit" do
    let(:attachment) { FactoryBot.create(:attachment) }
    before { authorize :update, attachment.attachable }
    it "should render edit attachment form" do
      get "/attachments/#{attachment.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update attachable" do
      before { unauthorize :update, attachment.attachable }
      it "should not render edit attachment form" do
        get "/attachments/#{attachment.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ATTACHMENT
  describe "PUT /attachments/:id" do
    let(:attachment) { FactoryBot.create(:attachment) }
    let(:new_label) { random_string }
    let(:new_description) { random_string }
    before { authorize(:update, attachment.attachable) }
    it "should update attachment" do
      put "/attachments/#{attachment.id}", params: {
        attachment: {
          label: new_label,
          description: new_description
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      attachment.reload
      expect(attachment.label).to eq(new_label)
      expect(attachment.description).to eq(new_description)
    end
    context "when validation fails" do
      it "shoud not update attachment" do
        expect {
          put "/attachments/#{attachment.id}", params: {
            attachment: {
              label: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { attachment.reload.label }
      end
    end
    context "when user is not authorized to update attachment's attachable" do
      before { unauthorize(:update, attachment.attachable) }
      it "should not authorize" do
        expect {
          put "/attachments/#{attachment.id}", params: {
            person: {
              label: new_label
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { attachment.reload.label }
      end
    end
  end

  # DOWNLOAD ATTACHMENT FILE
  describe "GET /attachments/:id/download" do
    let!(:attachment) { FactoryBot.create(:attachment) }
    before { authorize(:read, attachment.attachable) }
    it "should stream attachment file" do
      get "/attachments/#{attachment.id}/download"
      expect(response).to have_http_status(:ok)
    end
    context "when user is not authorized to read attachment's attachable" do
      before { unauthorize(:read, attachment.attachable) }
      it "should redirect to home page" do
        get "/attachments/#{attachment.id}/download"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

 # DESTROY ATTACHMENT
 describe "DELETE /attachments/:id" do
   let!(:attachment) { FactoryBot.create(:attachment) }
   before { authorize(:update, attachment.attachable) }
   it "should delete attachment" do
     expect {
       delete "/attachments/#{attachment.id}"
       expect(response).to have_http_status(:ok)
       expect(response).to render_template(:destroy)
     }.to change(Attachment, :count).from(1).to(0)
   end
   context "when user is not authorized to update attachment's attachable" do
     before { unauthorize(:update, attachment.attachable) }
     it "should not delete attachment" do
       expect {
         delete "/attachments/#{attachment.id}"
         expect(response).to have_http_status(:forbidden)
         expect(response).to render_template(:forbidden)
       }.not_to change(Attachment, :count)
     end
   end
 end
end
