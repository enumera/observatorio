describe "tags" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # TAGS INDEX
  describe "GET /tags" do
    let!(:suffix1) { "Puerto" }
    let!(:suffix2) { "Hablo" }

    let!(:tag1) { FactoryBot.create(:tag, name: "Tag A #{suffix1}", type: type) }
    let!(:tag2) { FactoryBot.create(:tag, name: "Tag B #{suffix1}", type: type) }
    let!(:tag3) { FactoryBot.create(:tag, name: "Tag C #{suffix2}", type: type) }
    let!(:tag4) { FactoryBot.create(:tag, name: "Tag D #{suffix2}", type: type) }
    let!(:tag5) { FactoryBot.create(:tag, name: "Tag E #{suffix2}", type: type) }
    let!(:tag6) { FactoryBot.create(:tag, name: "Tag F #{suffix2}", type: type) }
    let!(:tag7) { FactoryBot.create(:tag, name: "Tag G #{suffix2}", type: type) }
    let!(:tag8) { FactoryBot.create(:tag, name: "Tag H #{suffix2}", type: type) }
    let!(:tag9) { FactoryBot.create(:tag, name: "Tag I #{suffix2}", type: type) }
    let!(:tag10) { FactoryBot.create(:tag, name: "Tag J #{suffix2}", type: type) }
    let!(:tag11) { FactoryBot.create(:tag, name: "Tag K #{suffix2}", type: type) }
    let!(:tag12) { FactoryBot.create(:tag, name: "Tag L #{suffix2}", type: type) }

    before { authorize(:list, type) }

    [PersonTag, OrganizationTag, EventTag].each do |tag_klass|
      context "when type is #{tag_klass}" do
        let(:type) { tag_klass }

        it "should list only 8 tags per page" do
          get "/tags", params: {
            tag: { type: type }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:index)
          expect(response.body).to include(tag1.name)
          expect(response.body).to include(tag2.name)
          expect(response.body).to include(tag3.name)
          expect(response.body).to include(tag4.name)
          expect(response.body).to include(tag5.name)
          expect(response.body).to include(tag6.name)
          expect(response.body).to include(tag7.name)
          expect(response.body).to include(tag8.name)
          expect(response.body).not_to include(tag9.name)
          expect(response.body).not_to include(tag10.name)
          expect(response.body).not_to include(tag11.name)
          expect(response.body).not_to include(tag12.name)
        end
        it "should limit search results to 8 tags per page" do
          get "/tags", params: {
            tag: { type: type },
            search: suffix2
          }
          expect(response.body).to include(tag3.name)
          expect(response.body).to include(tag4.name)
          expect(response.body).to include(tag5.name)
          expect(response.body).to include(tag6.name)
          expect(response.body).to include(tag7.name)
          expect(response.body).to include(tag8.name)
          expect(response.body).to include(tag9.name)
          expect(response.body).to include(tag10.name)
          expect(response.body).not_to include(tag11.name)
          expect(response.body).not_to include(tag12.name)
        end
        it "should only return 2 tags" do
          get "/tags", params: {
            tag: { type: type },
            search: suffix1
          }
          expect(response.body).to include(tag1.name)
          expect(response.body).to include(tag2.name)
          expect(response.body).not_to include(tag3.name)
          expect(response.body).not_to include(tag4.name)
          expect(response.body).not_to include(tag5.name)
          expect(response.body).not_to include(tag6.name)
          expect(response.body).not_to include(tag7.name)
          expect(response.body).not_to include(tag8.name)
          expect(response.body).not_to include(tag9.name)
          expect(response.body).not_to include(tag10.name)
          expect(response.body).not_to include(tag11.name)
          expect(response.body).not_to include(tag12.name)
        end
        context "when user is not authorized to list tags" do
          before { unauthorize(:list, type) }
          it "should not list tags" do
            get "/tags", params: { tag: { type: type } }
            expect(response).to have_http_status(:forbidden)
            expect(response).to render_template(:forbidden)
          end
        end
      end
    end
  end

  # NEW TAG FORM
  describe "GET /tags/new" do
    let(:type) { Tag.subclasses.sample.name }
    before { authorize(:create, type.constantize) }
    it "should render create tag form" do
      get "/tags/new", params: {
        tag: { type: type }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create tags" do
      before { unauthorize(:create, type.constantize) }
      it "should not render create tag form" do
        get "/tags/new", params: {
          tag: { type: type }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE TAG
  describe "POST /tags" do
    let(:name) { random_string }
    let(:type) { Tag.subclasses.sample.name }
    before { authorize(:create, Tag) }
    it "should create person tag" do
      expect {
        post "/tags", params: {
          tag: {
            name: name,
            type: type
          }
        }
      }.to change(Tag, :count).from(0).to(1)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:create)
      created_tag = Tag.first
      expect(created_tag.name).to eq(name)
    end
    context "when validation fails" do
      it "should not create tag" do
        expect {
          post "/tags", params: {
            tag: {
              name: "",
              type: type
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Tag, :count)
      end
    end
    context "when user is not authorized to create tags" do
      before { unauthorize(:create, Tag) }
      it "should not create tag" do
        expect {
          post "/tags", params: {
            tag: {
              name: name,
              type: type
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Tag, :count)
      end
    end
  end

  # SHOW TAG
  describe "GET /tags/:id" do
    let(:person_tag) { Tag.find(FactoryBot.create(:tag, :person).id) }
    let(:organization_tag) { Tag.find(FactoryBot.create(:tag, :organization).id) }
    let(:event_tag) { Tag.find(FactoryBot.create(:tag, :event).id) }
    before do
      authorize(:read, person_tag)
      authorize(:read, organization_tag)
      authorize(:read, event_tag)
    end
    it "should show person tag" do
      get "/tags/#{person_tag.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    it "should show organization tag" do
      get "/tags/#{organization_tag.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    it "should show event tag" do
      get "/tags/#{event_tag.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the person tag" do
      let(:tag) { [person_tag, organization_tag, event_tag].sample }
      before { unauthorize(:read, tag) }
      it "should not show person tag" do
        get "/tags/#{tag.id}"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # SHOW TAG'S HISTORY
  describe "GET /tags/:id/history" do
    let!(:tag) { Tag.find(FactoryBot.create(:tag).id) }
    before { authorize :read, tag }
    it "should show tags's changes history" do
      get "/tags/#{tag.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read tag" do
      before { unauthorize :read, tag }
      it "should not show tags's changes history" do
        get "/tags/#{tag.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT TAG FORM
  describe "GET /tags/:id/edit" do
    let(:tag) { Tag.find(FactoryBot.create(:tag).id) }
    before { authorize(:update, tag) }
    it "should render update tag form" do
      get "/tags/#{tag.id}/edit", params: {
        tag: { type: tag.type }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update tag" do
      before { unauthorize(:update, tag) }
      it "should not render update tag form" do
        get "/tags/#{tag.id}/edit", params: {
          tag: { type: tag.type }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE TAG
  describe "PUT /tags/:id" do
    let!(:tag) { Tag.find(FactoryBot.create(:tag).id) }
    let(:name) { random_string }
    before { authorize(:update, tag) }
    it "should update tag" do
      put "/tags/#{tag.id}", params: {
        tag: {
          name: name,
          type: tag.type
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      tag.reload
      expect(tag.name).to eq(name)
    end
    context "when validation fails" do
      it "should not update person tag" do
        expect {
          put "/tags/#{tag.id}", params: {
            tag: {
              name: "",
              type: tag.type
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { tag.reload.name }
      end
    end
    context "when user is not authorized to update person tag" do
      before { unauthorize(:update, tag) }
      it "should not update person tag" do
        expect {
          put "/tags/#{tag.id}", params: {
            tag: {
              name: name,
              type: tag.type
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { tag.reload.name }
      end
    end
  end

  # DESTROY TAG
  describe "DELETE /tags/:id" do
    let!(:tag) { Tag.find(FactoryBot.create(:tag).id) }
    before { authorize(:delete, tag) }
    it "should delete person tag" do
      expect {
        delete "/tags/#{tag.id}", params: {
          tag: { type: tag.class.name }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Tag, :count).from(1).to(0)
    end
    context "when user is not authorized to delete the tag" do
      before { unauthorize(:delete, tag) }
      it "should not delete person tag" do
        expect {
          delete "/tags/#{tag.id}", params: {
            tag: { type: tag.class.name }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Tag, :count)
      end
    end
  end
end
