describe "preferences" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    authorize(:list, PaperTrail::Version)
  end

  # EDIT PREFERENCES PAGE
  describe "GET /preferences/edit" do
    it "should render edit preferences view" do
      get "/preferences/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end
  end

  # UPDATE PREFERENCES
  describe "PUT /preferences" do
    let(:time_zone) { (ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name) - [user.time_zone]).sample  }
    it "should update preferences" do
      expect {
        put "/preferences", params: {
          user: { time_zone: time_zone }
        }
        expect(response).to redirect_to("/")
      }.to change { user.reload.time_zone }.to(time_zone)
    end
    context "when validation fails" do
      it "should not update preferences" do
        expect {
          put "/preferences", params: {
            user: { time_zone: "" }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:edit)
        }.not_to change { user.reload.time_zone }
      end
    end
  end
end
