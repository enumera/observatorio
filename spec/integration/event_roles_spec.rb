describe "event roles" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # TAGS INDEX
  describe "GET /event_roles" do
    let!(:suffix1) { "Facility" }
    let!(:suffix2) { "Nature" }

    let!(:event_role1) { FactoryBot.create(:event_role, name: "Event Role A #{suffix1}") }
    let!(:event_role2) { FactoryBot.create(:event_role, name: "Event Role B #{suffix1}") }
    let!(:event_role3) { FactoryBot.create(:event_role, name: "Event Role C #{suffix2}") }
    let!(:event_role4) { FactoryBot.create(:event_role, name: "Event Role D #{suffix2}") }
    let!(:event_role5) { FactoryBot.create(:event_role, name: "Event Role E #{suffix2}") }
    let!(:event_role6) { FactoryBot.create(:event_role, name: "Event Role F #{suffix2}") }
    let!(:event_role7) { FactoryBot.create(:event_role, name: "Event Role G #{suffix2}") }
    let!(:event_role8) { FactoryBot.create(:event_role, name: "Event Role H #{suffix2}") }
    let!(:event_role9) { FactoryBot.create(:event_role, name: "Event Role I #{suffix2}") }
    let!(:event_role10) { FactoryBot.create(:event_role, name: "Event Role J #{suffix2}") }
    let!(:event_role11) { FactoryBot.create(:event_role, name: "Event Role K #{suffix2}") }
    let!(:event_role12) { FactoryBot.create(:event_role, name: "Event Role L #{suffix2}") }

    before { authorize(:list, EventRole) }
    it "should list only 8 event roles per page" do
      get "/event_roles"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(event_role1.name)
      expect(response.body).to include(event_role2.name)
      expect(response.body).to include(event_role3.name)
      expect(response.body).to include(event_role4.name)
      expect(response.body).to include(event_role5.name)
      expect(response.body).to include(event_role6.name)
      expect(response.body).to include(event_role7.name)
      expect(response.body).to include(event_role8.name)
      expect(response.body).not_to include(event_role9.name)
      expect(response.body).not_to include(event_role10.name)
      expect(response.body).not_to include(event_role11.name)
      expect(response.body).not_to include(event_role12.name)
    end
    it "should limit search results to 8 event roles per page" do
      get "/event_roles", params: { search: suffix2 }
      expect(response.body).to include(event_role3.name)
      expect(response.body).to include(event_role4.name)
      expect(response.body).to include(event_role5.name)
      expect(response.body).to include(event_role6.name)
      expect(response.body).to include(event_role7.name)
      expect(response.body).to include(event_role8.name)
      expect(response.body).to include(event_role9.name)
      expect(response.body).to include(event_role10.name)
      expect(response.body).not_to include(event_role11.name)
      expect(response.body).not_to include(event_role12.name)
    end
    it "should only return 2 event roles" do
      get "/event_roles", params: { search: suffix1 }
      expect(response.body).to include(event_role1.name)
      expect(response.body).to include(event_role2.name)
      expect(response.body).not_to include(event_role3.name)
      expect(response.body).not_to include(event_role4.name)
      expect(response.body).not_to include(event_role5.name)
      expect(response.body).not_to include(event_role6.name)
      expect(response.body).not_to include(event_role7.name)
      expect(response.body).not_to include(event_role8.name)
      expect(response.body).not_to include(event_role9.name)
      expect(response.body).not_to include(event_role10.name)
      expect(response.body).not_to include(event_role11.name)
      expect(response.body).not_to include(event_role12.name)
    end
    context "when user is not authorized to list roles" do
      before { unauthorize(:list, EventRole) }
      it "should not list roles" do
        get "/event_roles", params: { query: event_role1.name }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(event_role1.name)
        expect(response.body).not_to include(event_role1.id)
      end
    end
  end

  # NEW EVENT ROLE FORM
  describe "GET /event_roles/new" do
    before { authorize :create, EventRole }
    it "should render new role form" do
      get "/event_roles/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create roles" do
      before { unauthorize :create, EventRole }
      it "should not render new role form" do
        get "/event_roles/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EVENT ROLE
  describe "POST /ajax/event_roles" do
    let(:name) { random_string }
    before { authorize(:create, EventRole) }
    it "should create event role" do
      expect {
        post "/event_roles", params: {
          event_role: {
            name: name
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(EventRole, :count).from(0).to(1)
      created_role = EventRole.first
      expect(created_role.name).to eq(name)
    end
    context "when validation fails" do
      it "should not create role" do
        expect {
          post "/event_roles", params: {
            event_role: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(EventRole, :count)
      end
    end
    context "when user is not authorized to create person roles" do
      before { unauthorize(:create, EventRole) }
      it "should not create role" do
        expect {
          post "/event_roles", params: {
            event_role: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventRole, :count)
      end
    end
  end

  # SHOW EVENT ROLE
  describe "GET /event_roles/:id" do
    let(:event_role) { FactoryBot.create(:event_role) }
    before { authorize(:read, event_role) }
    it "should show event role" do
      get "/event_roles/#{event_role.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the event role" do
      before { unauthorize(:read, event_role) }
      it "should not show event role" do
        get "/event_roles/#{event_role.id}"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # SHOW EVENT ROLE'S HISTORY
  describe "GET /event_roles/:id/history" do
    let!(:event_role) { FactoryBot.create(:event_role) }
    before { authorize :read, event_role }
    it "should show event_roles's changes history" do
      get "/event_roles/#{event_role.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read event_role" do
      before { unauthorize :read, event_role }
      it "should not show event_roles's changes history" do
        get "/event_roles/#{event_role.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EVENT ROLE FORM
  describe "GET /event_roles/:id/edit" do
    let(:role) { FactoryBot.create(:event_role) }
    before { authorize :update, role }
    it "should render edit role form" do
      get "/event_roles/#{role.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit role" do
      before { unauthorize :update, role }
      it "should not render edit role form" do
        get "/event_roles/#{role.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EVENT ROLE
  describe "PUT /event_roles/:id" do
    let!(:event_role) { FactoryBot.create(:event_role) }
    let(:name) { random_string }
    before { authorize(:update, event_role) }
    it "should update event role" do
      put "/event_roles/#{event_role.id}", params: {
        event_role: {
          name: name
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      event_role.reload
      expect(event_role.name).to eq(name)
    end
    context "when validation fails" do
      it "should not update event role" do
        expect {
          put "/event_roles/#{event_role.id}", params: {
            event_role: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { event_role.reload.name }
      end
    end
    context "when user is not authorized to update event role" do
      before { unauthorize(:update, event_role) }
      it "should not update event role" do
        expect {
          put "/event_roles/#{event_role.id}", params: {
            event_role: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { event_role.reload.name }
      end
    end
  end

  # DESTROY TAG
  describe "DELETE /event_roles/:id" do
    let!(:event_role) { FactoryBot.create(:event_role) }
    before { authorize(:delete, event_role) }
    it "should delete event role" do
      expect {
        delete "/event_roles/#{event_role.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(EventRole, :count).from(1).to(0)
    end
    context "when dependent event participant" do
      before { FactoryBot.create(:event_participant, role: event_role) }
      it "should not delete event role" do
        expect {
          delete "/event_roles/#{event_role.id}"
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:error)
        }.not_to change(EventRole, :count)
      end
    end
    context "when user is not authorized to delete the role" do
      before { unauthorize(:delete, event_role) }
      it "should not delete event role" do
        expect {
          delete "/event_roles/#{event_role.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EventRole, :count)
      end
    end
  end
end
