describe "names" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # NAMES INDEX
  describe "GET /names" do
    let(:nameable) { FactoryBot.create(:person) }
    let!(:alternative_name1) {
      FactoryBot.create(:name, nameable: nameable, main: false)
    }
    let!(:alternative_name2) {
      FactoryBot.create(:name, nameable: nameable, main: false)
    }
    let!(:alternative_name3) {
      FactoryBot.create(:name, nameable: nameable, main: false)
    }
    let!(:other_name) { FactoryBot.create(:name) }
    before { authorize(:read, nameable) }
    it "should list names" do
      get "/names", params: {
        name: {
          nameable_id: nameable.id,
          nameable_type: nameable.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(nameable.main_name.value)
      expect(response.body).to include(alternative_name1.value)
      expect(response.body).to include(alternative_name2.value)
      expect(response.body).to include(alternative_name3.value)
      expect(response.body).not_to include(other_name.value)
    end
    it "should list alternative names" do
      get "/names", params: {
        name: {
          main: false,
          nameable_id: nameable.id,
          nameable_type: nameable.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(alternative_name1.value)
      expect(response.body).to include(alternative_name2.value)
      expect(response.body).to include(alternative_name3.value)
      expect(response.body).not_to include(nameable.main_name.value)
      expect(response.body).not_to include(other_name.value)
    end
  end

  # NEW NAME FORM
  describe "GET /names/new" do
    let!(:nameable) { FactoryBot.create(:person) }
    before { authorize(:update, nameable) }
    it "should render new name form" do
      get "/names/new", params: {
        name: {
          nameable_id: nameable.id,
          nameable_type: nameable.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update nameable" do
      before { unauthorize(:update, nameable) }
      it "should render new name form" do
        get "/names/new", params: {
          name: {
            nameable_id: nameable.id,
            nameable_type: nameable.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE NAME
  describe "POST /names" do
    let!(:nameable) { FactoryBot.create(:person) }
    let(:name_value) { random_string }
    before { authorize(:update, nameable) }
    it "should create name" do
      expect {
        post "/names", params: {
          name: {
            value: name_value,
            nameable_id: nameable.id,
            nameable_type: nameable.class
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Name, :count).from(1).to(2)
      created = Name.order(:created_at).last
      expect(created.value).to eq(name_value)
      expect(created.nameable).to eq(nameable)
    end
    context "when validation fails" do
      it "should not create name" do
        post "/names", params: {
          name: {
            value: "",
            nameable_id: nameable.id,
            nameable_type: nameable.class
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:form)
      end
    end
    context "when user is not authorized to update nameable" do
      before { unauthorize(:update, nameable) }
      it "should respond forbidden" do
        post "/names", params: {
          name: {
            value: name_value,
            nameable_id: nameable.id,
            nameable_type: nameable.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT NAME FORM
  describe "GET /names/:id/edit" do
    let(:nameable) { FactoryBot.create(:person) }
    let!(:name) { FactoryBot.create(:name, nameable: nameable) }
    before { authorize(:update, nameable) }
    it "should render edit name form" do
      get "/names/#{name.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update nameable" do
      before { unauthorize(:update, nameable) }
      it "should respond forbidden" do
        get "/names/#{name.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE NAME
  describe "PUT /names/:id" do
    let(:nameable) { FactoryBot.create(:person) }
    let!(:name) { FactoryBot.create(:name, nameable: nameable) }
    let(:new_value) { random_string }
    before { authorize(:update, nameable) }
    it "should update name" do
      expect {
        put "/names/#{name.id}", params: {
          name: {
            value: new_value
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update)
      }.to change { name.reload.value }.to(new_value)
    end
    context "when validation fails" do
      it "should not update" do
        expect {
          put "/names/#{name.id}", params: {
            name: {
              value: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { name.reload.value }
      end
    end
    context "when user is not authorized to update nameable" do
      before { unauthorize(:update, nameable) }
      it "should not update" do
        expect {
          put "/names/#{name.id}", params: {
            name: {
              value: new_value
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { name.reload.value }
      end
    end
  end

  # DESTROY NAME
  describe "DELETE /names/:id" do
    let(:nameable) { FactoryBot.create(:person) }
    let!(:name) { FactoryBot.create(:name, nameable: nameable) }
    before { authorize(:update, nameable) }
    it "should delete name" do
      expect {
        delete "/names/#{name.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Name, :count).from(2).to(1)
    end
    it "should not delete main name" do
      expect {
        delete "/names/#{nameable.main_name.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      }.not_to change(Name, :count)
    end
    context "when user is not authorized to update nameable" do
      before { unauthorize(:update, nameable) }
      it "should not delete" do
        expect {
          delete "/names/#{name.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Name, :count)
      end
    end
  end
end
