describe Differ do
  describe "#diff_by_line" do
    let(:original) { random_string }
    let(:current) { random_string }
    subject { Differ.diff_by_line(current, original).format_as(:html) }
    it { should eq "<span class='changeset delete'>#{original}</span>" \
                   "<span class='changeset insert'>#{current}</span>" }
  end

  describe "#diff_by_word" do
    let(:prefix) { random_string }
    let(:suffix) { random_string }
    let(:original_middle) { random_string }
    let(:current_middle) { random_string }
    let(:original) { "#{prefix} #{original_middle} #{suffix}" }
    let(:current) { "#{prefix} #{current_middle} #{suffix}" }
    subject { Differ.diff_by_word(current, original).format_as(:html) }
    it { should eq "#{prefix} " \
                   "<span class='changeset delete'>#{original_middle}</span>" \
                   "<span class='changeset insert'>#{current_middle}</span> "\
                   "#{suffix}" }
  end
end
