describe Downloader do
  describe "#get" do
    let(:url) { random_url }
    let(:visited_at) { random_datetime }
    let(:response_body) { random_string }
    let(:response_headers) do
      { "content-type" => "text/html;utf-8" }
    end
    let(:http_response) do
      double(
        status: 200,
        success?: true,
        body: response_body,
        headers: response_headers
      )
    end
    before do
      allow_any_instance_of(Faraday::Connection).to(
        receive(:get)
          .with(url)
          .and_return(http_response)
      )
      allow(DateTime).to receive(:now).and_return(visited_at)
      allow(http_response).to receive_message_chain(:env, :url)
                                .and_return(URI(url))
    end
    subject { DOWNLOADER.get(url) }
    it "should create tempfile with response body" do
      expect(subject.file.read).to eq(response_body)
    end
    it "should parse file extension" do
      expect(subject.extension).to eq(".html")
    end
    it "should set visited_at" do
      expect(subject.visited_at).to eq(visited_at)
    end
    context "when content-type is application/octet-stream" do
      let(:response_headers) do
        { "content-type" => "application/octet-stream",
          "content-disposition" => "attachment; filename=\"some_file.deb\"" }
      end
      it "should parse file extension" do
        expect(subject.extension).to eq(".deb")
      end
      context "and content-disposition filename has no quotes" do
        let(:response_headers) do
          { "content-type" => "application/octet-stream",
            "content-disposition" => "attachment; filename=some_file.deb" }
        end
        it "should parse file extension" do
          expect(subject.extension).to eq(".deb")
        end
      end
    end
    context "when content-type is video/mp4" do
      let(:url) { "#{random_url}/some-video.mp4" }
      let(:response_headers) do
        { "content-type" => "video/mp4" }
      end
      it "should parse file extension" do
        expect(subject.extension).to eq(".mp4")
      end
      context "and file has no extension in URL" do
        let(:url) { random_url }
        it "should parse file extension" do
          expect(subject.extension).to eq(".mp4")
        end
      end
    end
    context "when content-type is application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" do
      let(:url) { "#{random_url}/some-spreadsheet.xlsx" }
      let(:response_headers) do
        { "content-type" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }
      end
      it "should parse file extension" do
        expect(subject.extension).to eq(".xlsx")
      end
      context "and file has no extension in URL" do
        let(:url) { random_url }
        it "should parse file extension" do
          expect(subject.extension).to eq(".xlsx")
        end
      end
    end
    context "when HTTP response not successfull" do
      let(:http_response) do
        double(
          status: 404,
          success?: false,
          body: response_body,
          headers: response_headers
        )
      end
      let(:expected_visited_at) { random_datetime }
      before { allow(DateTime).to receive(:now).and_return(expected_visited_at) }
      it "should raise error" do
        expect { subject }.to raise_error(Downloader::NotFound)
      end
    end
    [401, 403].each do |status|
      context "when HTTP status is #{status}" do
        let(:http_response) do
          double(
            status: status,
            success?: false,
            body: response_body,
            headers: response_headers
          )
        end
        it "should raise error" do
          expect { subject }.to raise_error(Downloader::Forbidden)
        end
      end
    end
    context "when an error occurred" do
      before do
        allow_any_instance_of(Faraday::Connection).to(
          receive(:get)
            .with(url)
            .and_raise(Faraday::Error)
        )
      end
      it "should raise error" do
        expect { subject }.to raise_error(Downloader::NotFound)
      end
    end
  end

  describe "#head" do
    let(:url) { random_url }
    let(:visited_at) { random_datetime }
    let(:response_headers) do
      { "content-type" => "text/html;utf-8" }
    end
    let(:http_response) do
      double(
        status: 200,
        success?: true,
        body: "",
        headers: response_headers
      )
    end
    before do
      allow_any_instance_of(Faraday::Connection).to(
        receive(:head)
          .with(url)
          .and_return(http_response)
      )
      allow(DateTime).to receive(:now).and_return(visited_at)
      allow(http_response).to receive_message_chain(:env, :url)
                                .and_return(URI(url))
    end
    subject { DOWNLOADER.head(url) }
    it "should respond empty file" do
      expect(subject.file.read).to be_empty
    end
    it "should parse file extension" do
      expect(subject.extension).to eq(".html")
    end
    it "should set visited_at" do
      expect(subject.visited_at).to eq(visited_at)
    end
    context "when content-type is application/octet-stream" do
      let(:response_headers) do
        { "content-type" => "application/octet-stream",
          "content-disposition" => "attachment; filename=\"some_file.deb\"" }
      end
      it "should parse file extension" do
        expect(subject.extension).to eq(".deb")
      end
    end
    context "when HTTP response is forbidden" do
      let(:http_response) do
        double(
          status: 403,
          success?: false,
          headers: response_headers
        )
      end
      let(:expected_visited_at) { random_datetime }
      before { allow(DateTime).to receive(:now).and_return(expected_visited_at) }
      it "should raise error" do
        expect { subject }.to raise_error(Downloader::Forbidden)
      end
    end
    context "when an error occurred" do
      before do
        allow_any_instance_of(Faraday::Connection).to(
          receive(:head)
            .with(url)
            .and_raise(Faraday::Error)
        )
      end
      it "should raise error" do
        expect { subject }.to raise_error(Downloader::NotFound)
      end
    end
  end
end
