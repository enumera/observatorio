describe HtmlToPdf do
  describe "#convert" do
    let(:url) { random_url }
    let(:filepath) { File.join(Rails.root, "spec", "files", "beethoven.pdf") }
    let(:expected_file) { File.new(filepath, encoding: "ASCII-8BIT") }
    let(:visited_at) { random_datetime }
    let(:pdfkit) do
      double(to_pdf: File.new(filepath, encoding: "ASCII-8BIT").read)
    end
    subject { HTML_TO_PDF.convert(url) }
    before do
      allow(DateTime).to receive(:now).and_return(visited_at)
      allow(PDFKit).to receive(:new)
                         .with(url, {})
                         .and_return(pdfkit)
    end
    it "should create tempfile with pdf data" do
      expect(subject.file.read).to eq(expected_file.read)
    end
    it "should create tempfile with .pdf extension" do
      expect(subject.extension).to eq(".pdf")
    end
    it "should set visited_at" do
      expect(subject.visited_at).to eq(visited_at)
    end
    context "when conversion fails" do
      before do
        allow(PDFKit).to receive(:new)
                           .with(url, {})
                           .and_raise(RuntimeError)
        allow(PDFKit).to receive(:new)
                          .with(url, disable_javascript: true)
                          .and_return(pdfkit)
      end
      it "should retry disabling JavaScript" do
        expect(subject.file.read).to eq(expected_file.read)
        expect(subject.extension).to eq(".pdf")
        expect(subject.visited_at).to eq(visited_at)
      end
      context "when conversion fails even with no JavaScript" do
        before do
          allow(PDFKit).to receive(:new)
                            .with(url, disable_javascript: true)
                            .and_raise(RuntimeError)
        end
        it "should raise exception" do
          expect { subject }.to raise_error(HtmlToPdf::Error)
        end
      end
    end
  end
end
