describe Ability do
  describe "on Person" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Person" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Person) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Person" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:person) { FactoryBot.create(:person) }
        subject { described_class.new(user).can?(object_action.to_sym, person) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Organization" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Organization" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Organization) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Organization" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:organization) { FactoryBot.create(:organization) }
        subject { described_class.new(user).can?(object_action.to_sym, organization) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on OrganizationPosition" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "OrganizationPosition" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, OrganizationPosition) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "OrganizationPosition" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:position) { FactoryBot.create(:organization_position) }
        subject { described_class.new(user).can?(object_action.to_sym, position) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on EducationCourse" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "EducationCourse" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, EducationCourse) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "EducationCourse" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:course) { FactoryBot.create(:education_course) }
        subject { described_class.new(user).can?(object_action.to_sym, course) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on EducationDegree" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "EducationDegree" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, EducationDegree) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "EducationDegree" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:degree) { FactoryBot.create(:education_degree) }
        subject { described_class.new(user).can?(object_action.to_sym, degree) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Event" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Event" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Event) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Event" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:event) { FactoryBot.create(:event) }
        subject { described_class.new(user).can?(object_action.to_sym, event) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on EventRole" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "EventRole" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, EventRole) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "EventRole" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:role) { FactoryBot.create(:event_role) }
        subject { described_class.new(user).can?(object_action.to_sym, role) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Tag" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Tag" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Tag) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Tag" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:tag) { FactoryBot.create(:tag) }
        subject { described_class.new(user).can?(object_action.to_sym, tag) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Location" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Location" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Location) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Location" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:location) { FactoryBot.create(:location) }
        subject { described_class.new(user).can?(object_action.to_sym, location) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on UserGroup" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "UserGroup" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, UserGroup) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "UserGroup" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:group) { FactoryBot.create(:user_group) }
        subject { described_class.new(user).can?(object_action.to_sym, group) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Import" do
    context "list" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: { "Import" => ["list"] })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      subject { described_class.new(user).can?(:list, Import) }
      it { should be true }
      context "when user is not in a group giving permissions" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "read", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Import" => [object_action] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:import) { FactoryBot.create(:memberships_import) }
        subject { described_class.new(user).can?(object_action.to_sym, import) }
        it { should be true }
        context "when user is not in a group giving permissions" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Name" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:nameable) { FactoryBot.create(:person) }
      let(:name) { FactoryBot.create(:name, nameable: nameable) }
      subject { described_class.new(user).can?(:read, name) }
      it { should be true }
      context "when user is not in a group giving permissions to read nameable" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    context "update" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["update"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:nameable) { FactoryBot.create(:person) }
      let(:name) { FactoryBot.create(:name, nameable: nameable) }
      subject { described_class.new(user).can?(:update, name) }
      it { should be true }
      context "when user is not in a group giving permissions to update nameable" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:nameable) { FactoryBot.create(:person) }
        let(:name) { FactoryBot.create(:name, nameable: nameable) }
        subject { described_class.new(user).can?(object_action.to_sym, name) }
        it { should be true }
        context "when main name" do
          subject { described_class.new(user).can?(object_action.to_sym, nameable.main_name) }
          it { should be false }
        end
        context "when user is not in a group giving permissions to update nameable" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Picture" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:picturable) { FactoryBot.create(:person) }
      let(:picture) { FactoryBot.create(:picture, picturable: picturable) }
      subject { described_class.new(user).can?(:read, picture) }
      it { should be true }
      context "when user is not in a group giving permissions to read picturable" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:picturable) { FactoryBot.create(:person) }
        let(:picture) { FactoryBot.create(:picture, picturable: picturable) }
        subject { described_class.new(user).can?(object_action.to_sym, picture) }
        it { should be true }
        context "when user is not in a group giving permissions to update picturable" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Link" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:linkable) { FactoryBot.create(:person) }
      let(:link) { FactoryBot.create(:link, linkable: linkable) }
      subject { described_class.new(user).can?(:read, link) }
      it { should be true }
      context "when user is not in a group giving permissions to read linkable" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:linkable) { FactoryBot.create(:person) }
        let(:link) { FactoryBot.create(:link, linkable: linkable) }
        subject { described_class.new(user).can?(object_action.to_sym, link) }
        it { should be true }
        context "when user is not in a group giving permissions to update linkable" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Attachment" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:attachable) { FactoryBot.create(:person) }
      let(:attachment) { FactoryBot.create(:attachment, attachable: attachable) }
      subject { described_class.new(user).can?(:read, attachment) }
      it { should be true }
      context "when user is not in a group giving permissions to read attachable" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:attachable) { FactoryBot.create(:person) }
        let(:attachment) { FactoryBot.create(:attachment, attachable: attachable) }
        subject { described_class.new(user).can?(object_action.to_sym, attachment) }
        it { should be true }
        context "when user is not in a group giving permissions to update attachable" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Relationship" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:relationship) { FactoryBot.create(:relationship) }
      subject { described_class.new(user).can?(:read, relationship) }
      it { should be true }
      context "when user is not in a group giving permissions to read people" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: { "Person" => ["update"] })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:relationship) { FactoryBot.create(:relationship) }
        subject { described_class.new(user).can?(object_action.to_sym, relationship) }
        it { should be true }
        context "when user is not in a group giving permissions to update people" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Education" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"], "Organization" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:education) { FactoryBot.create(:education) }
      subject { described_class.new(user).can?(:read, education) }
      it { should be true }
      context "when user is not in a group giving permissions to read people & organizations" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"], "Organization" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:education) { FactoryBot.create(:education) }
        subject { described_class.new(user).can?(object_action.to_sym, education) }
        it { should be true }
        context "when user is not in a group giving permissions to update people & organizations" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on OrganizationDepartment" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Organization" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:department) { FactoryBot.create(:organization_department) }
      subject { described_class.new(user).can?(:read, department) }
      it { should be true }
      context "when user is not in a group giving permissions to read organizations" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Organization" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:department) { FactoryBot.create(:organization_department) }
        subject { described_class.new(user).can?(object_action.to_sym, department) }
        it { should be true }
        context "when user is not in a group giving permissions to update organizations" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
        context "when department has parent" do
          let(:child_department) { FactoryBot.build(:organization_department, parent: department, organization: nil) }
          subject { described_class.new(user).can?(object_action.to_sym, child_department) }
          it { should be true }
          context "when user is not in a group giving permissions to update organizations" do
            let(:user) { FactoryBot.create(:user) }
            it { should be false }
          end
        end
      end
    end
  end

  describe "on OrganizationMember" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"], "Organization" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:organization_member) { FactoryBot.create(:organization_member) }
      subject { described_class.new(user).can?(:read, organization_member) }
      it { should be true }
      context "when user is not in a group giving permissions to read people & organizations" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"], "Organization" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:organization_member) { FactoryBot.create(:organization_member) }
        subject { described_class.new(user).can?(object_action.to_sym, organization_member) }
        it { should be true }
        context "when user is not in a group giving permissions to update people & organizations" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on OrganizationOwner" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"], "Organization" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:organization_owner) { FactoryBot.create(:organization_owner) }
      subject { described_class.new(user).can?(:read, organization_owner) }
      it { should be true }
      context "when user is not in a group giving permissions to read people & organizations" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"], "Organization" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:organization_owner) { FactoryBot.create(:organization_owner) }
        subject { described_class.new(user).can?(object_action.to_sym, organization_owner) }
        it { should be true }
        context "when user is not in a group giving permissions to update people & organizations" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on EventParticipant" do
    context "read" do
      let(:user_group) do
        FactoryBot.create(:user_group, permissions: {
          "Person" => ["read"], "Organization" => ["read"], "Event" => ["read"]
        })
      end
      let(:user) { FactoryBot.create(:user, groups: [user_group]) }
      let(:event_participant) { FactoryBot.create(:event_participant) }
      subject { described_class.new(user).can?(:read, event_participant) }
      it { should be true }
      context "when user is not in a group giving permissions to read people, organizations & events" do
        let(:user) { FactoryBot.create(:user) }
        it { should be false }
      end
    end
    ["create", "update", "delete"].each do |object_action|
      context object_action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["update"], "Organization" => ["update"], "Event" => ["update"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:event_participant) { FactoryBot.create(:event_participant) }
        subject { described_class.new(user).can?(object_action.to_sym, event_participant) }
        it { should be true }
        context "when user is not in a group giving permissions to update people, organizations & events" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
  end

  describe "on Note" do
    ["read", "create"].each do |action|
      context action do
        let(:user_group) do
          FactoryBot.create(:user_group, permissions: {
            "Person" => ["read"]
          })
        end
        let(:user) { FactoryBot.create(:user, groups: [user_group]) }
        let(:person) { FactoryBot.create(:person) }
        let(:note) { FactoryBot.create(:note, subject: person) }
        subject { described_class.new(user).can?(action.to_sym, note) }
        it { should be true }
        context "when user is not un group allowing to read people" do
          let(:user) { FactoryBot.create(:user) }
          it { should be false }
        end
      end
    end
    context "delete" do
      let(:user) { FactoryBot.create(:user) }
      let(:note) { FactoryBot.create(:note, author: user) }
      subject { described_class.new(user).can?(:delete, note) }
      it { should be true }
      context "when user is not note's author" do
        let(:note) { FactoryBot.create(:note) }
        it { should be false }
      end
    end
  end
end
