describe HasFile do
  class described_class::TestModel < ApplicationRecord
    has_file mandatory: true
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.text :file
    t.text :file_sha2_hex
    t.text :file_md5_hex
    t.text :file_extension
    t.text :file_encryption_iv_base64
    t.integer :file_size
  end
  subject { described_class::TestModel.new }

  it { should validate_presence_of :file }
  it { should validate_presence_of :file_size }
  it { should validate_presence_of :file_encryption_iv_base64 }

  describe "#file_unencrypted=" do
    let(:file) { File.new(File.join(Rails.root, "spec", "files", "text.txt")) }
    let(:encryption_iv) { Base64.decode64("APSVOGznteUFBnrZoRWbTw==") }
    before do
      allow(CIPHER).to receive(:random_iv).and_return(encryption_iv)
    end
    let(:test_model) { described_class::TestModel.new }
    subject { lambda { test_model.file_unencrypted = file } }
    it { should change(test_model, :file_size).from(nil).to(11) }
    it { should change(test_model, :file_md5_hex).from(nil).to("e5a4aadf9b0494a7342434c45439cbf7") }
    it { should change(test_model, :file_sha2_hex).from(nil).to("475350d965c3720880b2d20aab83226512c0fec59c38737434b01071b6aaf46d") }
    it { should change(test_model, :file_decrypted_base64).from(nil).to("Y2xlYXIgdGV4dAo=") }
  end

  describe "#file_decrypted_base64=" do
    let(:file) { File.new(File.join(Rails.root, "spec", "files", "text.txt")) }
    let(:encryption_iv) { Base64.decode64("APSVOGznteUFBnrZoRWbTw==") }
    before do
      allow(CIPHER).to receive(:random_iv).and_return(encryption_iv)
    end
    let(:test_model) { described_class::TestModel.new }
    subject { lambda { test_model.file_unencrypted_base64 = Base64.encode64(file.read) } }
    it { should change(test_model, :file_size).from(nil).to(11) }
    it { should change(test_model, :file_md5_hex).from(nil).to("e5a4aadf9b0494a7342434c45439cbf7") }
    it { should change(test_model, :file_sha2_hex).from(nil).to("475350d965c3720880b2d20aab83226512c0fec59c38737434b01071b6aaf46d") }
    it { should change(test_model, :file_decrypted_base64).from(nil).to("Y2xlYXIgdGV4dAo=") }
  end

  describe "#file_html?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".html") }
    subject { test_model.file_html? }
    it { should be(true) }
    context "when content type is not HTML" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".mp3") }
      it { should be(false) }
    end
  end

  describe "#file_pdf?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".pdf") }
    subject { test_model.file_pdf? }
    it { should be(true) }
    context "when content type is not PDF" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".mp3") }
      it { should be(false) }
    end
  end

  describe "#file_image?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".jpg") }
    subject { test_model.file_image? }
    it { should be(true) }
    context "when content type is not an image" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".mp3") }
      it { should be(false) }
    end
  end

  describe "#file_video?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".mp4") }
    subject { test_model.file_video? }
    it { should be(true) }
    context "when content type is not an image" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".mp3") }
      it { should be(false) }
    end
  end

  describe "#file_audio?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".mp3") }
    subject { test_model.file_audio? }
    it { should be(true) }
    context "when content type is not an audio" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".doc") }
      it { should be(false) }
    end
  end

  describe "#file_text?" do
    let(:test_model) { described_class::TestModel.new(file_extension: ".txt") }
    subject { test_model.file_text? }
    it { should be(true) }
    context "when content type is not an audio" do
      let(:test_model) { described_class::TestModel.new(file_extension: ".doc") }
      it { should be(false) }
    end
  end
end
