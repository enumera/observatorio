describe HasTimeZone do
  class described_class::TestModel < ApplicationRecord
    has_time_zone
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.text :time_zone
  end
  subject { described_class::TestModel.new }

  it { should validate_inclusion_of(:time_zone).in_array(ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name)) }

  describe "#time_zone" do
    let(:default_time_zone) { ActiveSupport::TimeZone.all.sample }
    before { allow(Time).to receive(:zone).and_return(default_time_zone) }
    subject { described_class::TestModel.new.time_zone }
    # by default
    it { should eq(default_time_zone.tzinfo.name) }
  end
end
