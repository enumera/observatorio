describe HasNameAndDisplayPriority do
  class described_class::TestModel < ApplicationRecord
    has_name_and_display_priority
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.text    :name
    t.integer :display_priority
  end
  subject { described_class::TestModel.new }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:display_priority) }

  it { expect(subject.display_priority).to be(0) }

  it "should strip name" do
    subject.name = " name "
    expect { subject.validate }.to change(subject, :name).to("name")
  end

  describe ".default_scope" do
    let!(:model1) { described_class::TestModel.create!(name: "name1") }
    let!(:model2) { described_class::TestModel.create!(name: "name2") }
    let!(:model3) { described_class::TestModel.create!(name: "aaa", display_priority: 1) }
    subject { described_class::TestModel.all }

    it { expect(subject[0]).to eq(model1) }
    it { expect(subject[1]).to eq(model2) }
    it { expect(subject[2]).to eq(model3) }
  end

  describe ".search" do
    let(:model1) { described_class::TestModel.create!(name: "some_strange_name") }
    let(:model2) { described_class::TestModel.create!(name: "some_different_name") }
    let(:model3) { described_class::TestModel.create!(name: "some_strange_different_name") }
    it "should search by name" do
      expect(described_class::TestModel.search("some")).to contain_exactly(model1, model2, model3)
      expect(described_class::TestModel.search("name")).to contain_exactly(model1, model2, model3)
      expect(described_class::TestModel.search("strange")).to contain_exactly(model1, model3)
      expect(described_class::TestModel.search("different")).to contain_exactly(model2, model3)
      expect(described_class::TestModel.search("strange different")).to contain_exactly(model3)
      expect(described_class::TestModel.search("different strange")).to contain_exactly(model3)
    end
  end
end
