describe DateBoundaries do
  class described_class::TestModel < ApplicationRecord
    has_date_boundaries
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.timestamps                    null: false
    t.date       :from_date
    t.date       :to_date
    t.boolean :approximate_dates
  end
  subject { described_class::TestModel.new }

  it "should validate date boundaries" do
    subject.approximate_dates = random_bool
    before = Date.today - rand(1..10).days
    after = Date.today
    subject.from_date = nil
    subject.to_date = nil
    expect(subject).to be_valid

    subject.from_date = before
    subject.to_date = nil
    subject.approximate_dates = random_bool
    expect(subject).to be_valid

    subject.from_date = nil
    subject.to_date = after
    subject.approximate_dates = random_bool
    expect(subject).to be_valid

    subject.from_date = before
    subject.to_date = after
    subject.approximate_dates = random_bool
    expect(subject).to be_valid

    subject.from_date = after
    subject.to_date = before
    subject.approximate_dates = random_bool
    expect(subject).not_to be_valid
  end

  it "should validate no date is in the future" do
    subject.from_date = Date.today + 1.day
    expect(subject).not_to be_valid
    subject.from_date = nil
    subject.to_date = Date.today + 1.day
    expect(subject).not_to be_valid
  end

  context "when from_date is set" do
    it "should validate presence of approximate_dates" do
      subject.from_date = Date.today - 1.day
      subject.to_date = nil
      subject.approximate_dates = nil
      expect(subject).not_to be_valid
      subject.approximate_dates = random_bool
      expect(subject).to be_valid
    end
  end

  context "when to_date is set" do
    it "should validate presence of approximate_dates" do
      subject.from_date = nil
      subject.to_date = Date.today - 1.day
      subject.approximate_dates = nil
      expect(subject).not_to be_valid
      subject.approximate_dates = random_bool
      expect(subject).to be_valid
    end
  end

  context "when neither from_date nor to_date is set" do
    it "should clear approximate_dates" do
      subject.from_date = nil
      subject.to_date = nil
      subject.approximate_dates = random_bool
      expect(subject).to be_valid
      expect(subject.approximate_dates).to be_nil
    end
  end
end
