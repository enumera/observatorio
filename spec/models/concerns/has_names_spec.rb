describe HasNames do
  class described_class::TestModel < ApplicationRecord
    has_names
  end
  create_test_model_table described_class::TestModel, id: :uuid
  subject { described_class::TestModel.new }

  it { should validate_presence_of :main_name }

  it { should have_many(:names)
                .class_name("Name")
                .inverse_of(:nameable)
                .dependent(:destroy) }

  it { should have_one(:main_name)
                .class_name("Name")
                .inverse_of(:nameable)
                .dependent(:destroy)
                .conditions(main: true)
                .validate(true)
                .autosave(true) }

  it { should have_many(:alternative_names)
                .class_name("Name")
                .inverse_of(:nameable)
                .dependent(:destroy)
                .conditions(main: false) }

  describe "#name=" do
    let(:name_value) { random_string }
    let(:model) { described_class::TestModel.new(name: name_value) }
    it "should set main name value" do
      expect(model.main_name).to be_present
      expect(model.main_name.value).to eq(name_value)
    end
    context "when name is already persisted" do
      let(:model) { described_class::TestModel.create!(name: random_string) }
      it "should update main name value" do
        model.name = name_value
        expect(model.main_name.value).to eq(name_value)
      end
    end
  end

  describe "#name" do
    let(:name_value) { random_string }
    let(:model) { described_class::TestModel.create!(name: name_value) }
    subject { model.name }
    it { should eq(name_value) }
  end

  describe ".search" do
    let!(:model1) { described_class::TestModel.create!(name: "Rote Armee Fraktion") }
    let!(:model2) { described_class::TestModel.create!(name: "Action Directe") }
    it "should search by name" do
      expect(described_class::TestModel.search("armee")).to contain_exactly(model1)
      expect(described_class::TestModel.search("direct")).to contain_exactly(model2)
      expect(described_class::TestModel.search("tion")).to contain_exactly(model1, model2)
    end
  end
end
