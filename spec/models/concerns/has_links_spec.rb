describe HasLinks do
  class described_class::TestModel < ApplicationRecord
    has_links
  end
  create_test_model_table described_class::TestModel, id: :uuid
  subject { described_class::TestModel.new }

  it { should have_many(:links).dependent(:destroy) }

  it "should validate uniqueness of links label" do
    label = random_string
    subject.assign_attributes(
      links: [
        FactoryBot.build(:link, label: label),
        FactoryBot.build(:link, label: label.swapcase)
      ]
    )
    expect(subject).not_to be_valid
    expect(subject.links[0].errors[:label]).to be_empty
    expect(subject.links[1].errors[:label]).not_to be_empty
  end
end
