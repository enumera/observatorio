describe Relationship do
  let(:owner) { FactoryBot.create(:person) }
  let(:with) { FactoryBot.create(:person) }
  subject { FactoryBot.build(:relationship, owner: owner, with: with) }

  it { should validate_presence_of(:owner) }
  it { should validate_presence_of(:with) }
  it { should validate_inclusion_of(:degree).in_array(%w(child parent partner sibling)) }

  it "should require case sensitive unique value for with_id scoped to owner_id" do
    FactoryBot.create(:relationship, with: subject.with)
    expect(subject).to be_valid
    FactoryBot.create(:relationship, owner: subject.owner)
    expect(subject).to be_valid
    FactoryBot.create(:relationship, with: subject.with, owner: subject.owner)
    expect(subject).not_to be_valid
  end

  describe "#save" do
    context "on create" do
      it "should create inverse relationship" do
        relationship = FactoryBot.create(:relationship)
        inverse = Relationship.find_by(owner_id: relationship.with_id,
                                       with_id: relationship.owner_id)
        expect(inverse).to be_present
        expect(inverse.degree).to eq(relationship.inverse_relationship_degree)
      end
      it "should create inverse relationship with same date boundaries" do
        relationship = FactoryBot.create(:relationship_date_boundaried)
        inverse = Relationship.find_by(owner_id: relationship.with_id,
                                       with_id: relationship.owner_id)
        expect(inverse).to be_present
        expect(inverse.degree).to eq(relationship.inverse_relationship_degree)
      end
    end
    context "on update" do
      let(:relationship) { FactoryBot.create(:relationship) }
      let(:new_degree) { (Relationship::DEGREES - [relationship.degree]).sample }
      it "should update inverse relationship" do
        relationship.update_attributes!(degree: new_degree)
        inverse = Relationship.find_by(owner_id: relationship.with_id,
                                       with_id: relationship.owner_id)
        expect(inverse.degree).to eq(relationship.inverse_relationship_degree)
      end
    end
  end

  describe "#destroy" do
    let(:relationship) { FactoryBot.create(:relationship) }
    it "should destroy inverse relationship" do
      relationship.destroy!
      inverse = Relationship.find_by(owner_id: relationship.with_id,
                                     with_id: relationship.owner_id)
      expect(inverse).not_to be_present
    end
  end

  describe "#inverse_relationship_degree" do
    let(:relationship) { FactoryBot.build(:relationship, degree: degree) }
    subject { relationship.inverse_relationship_degree }
    %w(partner sibling).each do |degree|
      context "when degree is #{degree}" do
        let(:degree) { degree }
        it { should eq(degree) }
      end
    end
    context "when degree is child" do
      let(:degree) { "child" }
      it { should eq("parent") }
    end
    context "when degree is parent" do
      let(:degree) { "parent" }
      it { should eq("child") }
    end
  end
end
