describe OrganizationOwner do
  let(:organization) { FactoryBot.create(:organization) }
  let(:owner) { FactoryBot.create(:person) }
  subject { FactoryBot.build(:organization_owner, organization: organization, owner: owner) }

  it { should validate_presence_of(:owner) }
  it { should validate_presence_of(:organization) }
  it { should validate_presence_of(:portion) }
  it { should validate_numericality_of(:portion).is_greater_than(0.0).is_less_than_or_equal_to(1.0) }

  it "should require unique value for organization_id scoped to owner_id" do
    FactoryBot.create(:organization_owner, organization: subject.organization)
    expect(subject).to be_valid
    FactoryBot.create(:organization_owner, owner: subject.owner)
    expect(subject).to be_valid
    FactoryBot.create(:organization_owner, organization: subject.organization, owner: subject.owner)
    expect(subject).not_to be_valid
  end

  it "should validate organization cumulated organization_owner portion is lower or equal than 1.0" do
    organization_owner = FactoryBot.create(:organization_owner)
    subject.organization = organization_owner.organization.reload
    subject.portion = 1.0
    expect(subject).not_to be_valid
  end

  it "should validate organization doest not own itself" do
    subject.owner = subject.organization
    expect(subject).not_to be_valid
  end

  describe "#portion_percentage" do
    let(:percentage) { random_int }
    let(:organization_owner) { FactoryBot.build(:organization_owner) }
    subject { lambda { organization_owner.portion_percentage = percentage } }
    it { should change(organization_owner, :portion).to (percentage.to_f / 100) }
    context "when percentage is decimal" do
      let(:percentage) { 23.7 }
      it { should change(organization_owner, :portion).to(0.23) }
    end
    context "when percentage is nil" do
      let(:percentage) { nil }
      it { should change(organization_owner, :portion).to(nil) }
    end
  end
end
