describe Picture do
  subject { FactoryBot.build(:picture) }

  it { should validate_presence_of(:file) }
  it { should validate_presence_of(:picturable) }
  it { should validate_uniqueness_of(:picturable_id)
                .scoped_to(:picturable_type)
                .case_insensitive }

  describe "#source" do
    let(:source_url) { random_url }
    let(:source_text) { random_string }
    let(:picture) { FactoryBot.build(:picture,
                                     source_url: source_url,
                                     source_text: source_text) }
    subject { picture.source }
    it { should eq(source_url) }
    context "when source_url is not given" do
      let(:source_url) { nil }
      it { should eq(source_text) }
    end
  end

  describe "#save" do
    subject { lambda { picture.save } }
    context "when source_url is set" do
      let(:source_url) { random_url }
      let(:head) { double(extension: ".jpg", success?: true) }
      let(:get) do
        double(
          extension: ".jpg",
          file: File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg"))
        )
      end
      let!(:picture) { FactoryBot.build(:picture,
                                        file: nil,
                                        source_url: source_url,
                                        source_text: random_string) }
      let!(:picture_id) { SecureRandom.uuid }
      before do
        allow(DOWNLOADER).to receive(:head)
                               .with(source_url)
                               .and_return(head)
        allow(DOWNLOADER).to receive(:get)
                               .with(source_url)
                               .and_return(get)
        allow_any_instance_of(CloudinaryUploader).to receive(:picture_id)
                                                       .and_return(picture_id)
      end
      it { should change(picture, :source_text).to(nil) }
      it { should change { picture.file.url }.to(
        "https://res.cloudinary.com/observatorio-mock/image/upload/v1/observatorio/picture/file/#{picture_id}.jpg"
        ) }
      context "when URL not successful" do
        let(:head) { double(extension: ".jpg", success?: false) }
        it { should_not change { picture.file.url } }
        it { should change { picture.errors[:source_url] }.to([I18n.t("errors.messages.invalid")]) }
      end
      context "when URL is not an picture" do
        let(:head) { double(extension: ".deb", success?: true) }
        it { should_not change { picture.file.url } }
        it { should change { picture.errors[:source_url] }.to([I18n.t("errors.messages.invalid")]) }
      end
    end
  end
end
