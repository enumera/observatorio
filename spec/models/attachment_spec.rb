describe Attachment do
  subject { FactoryBot.build(:attachment) }

  it { should belong_to :attachable }

  it { should validate_presence_of :label }
  it { should validate_uniqueness_of(:label)
                .scoped_to(:attachable_id, :attachable_type)
                .case_insensitive }

  describe "#save" do
    context "when attachable is a Relationship" do
      let(:relationship) { FactoryBot.create(:relationship) }
      context "on create" do
        it "should create attachment on inverse relationship" do
          attachment = FactoryBot.create(:attachment, attachable: relationship)
          inverse_attachment = Attachment.find_by(attachable: relationship.inverse_relationship)
          expect(inverse_attachment).to be_present
          expect(inverse_attachment.label).to eq(attachment.label)
          expect(inverse_attachment.file.read).to eq(attachment.file.read)
          expect(inverse_attachment.file_size).to eq(attachment.file_size)
          expect(inverse_attachment.file_extension).to eq(attachment.file_extension)
          expect(inverse_attachment.file_encryption_iv_base64).to (
            eq(attachment.file_encryption_iv_base64)
          )
        end
      end
      context "on update" do
        let(:attachment) { FactoryBot.create(:attachment, attachable: relationship) }
        let(:new_label) { random_string }
        it "should update attachment on inverse relationship" do
          attachment.update_attributes!(label: new_label)
          inverse_attachment = Attachment.find_by(attachable: relationship.inverse_relationship)
          expect(inverse_attachment).to be_present
          expect(inverse_attachment.label).to eq(new_label)
        end
      end
      context "on destroy" do
        let!(:attachment) { FactoryBot.create(:attachment, attachable: relationship) }
        it "should destroy link on inverse relationship" do
          attachment.destroy!
          inverse_attachment = Attachment.find_by(attachable: relationship.inverse_relationship)
          expect(inverse_attachment).not_to be_present
        end
      end
    end
  end
end
