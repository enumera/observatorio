describe Tag do
  subject { FactoryBot.build(:tag) }

  it { should validate_inclusion_of(:type).in_array(Tag.subclasses.map(&:name)) }
end
