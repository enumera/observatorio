describe OrganizationDepartment do
  subject { FactoryBot.build(:organization_department) }

  it { should have_many(:members)
                .class_name(OrganizationMember)
                .inverse_of(:department)
                .with_foreign_key("department_id")
                .dependent(:nullify) }
  it { should have_many(:children)
                .class_name(OrganizationDepartment)
                .inverse_of(:parent)
                .with_foreign_key("parent_id")
                .dependent(:destroy) }

  it { should validate_presence_of(:organization) }
  it { should validate_uniqueness_of(:name).scoped_to(:organization_id).case_insensitive }

  describe "#save" do
    let!(:parent) { FactoryBot.create(:organization_department) }
    let!(:organization_department) { FactoryBot.build(:organization_department, parent: parent) }
    subject { lambda { organization_department.save! } }
    it { should change(organization_department, :organization_id).to(parent.organization_id) }
  end
end
