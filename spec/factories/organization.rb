FactoryBot.define do
  factory :organization do
    trait :school do
      school { true }
    end
    trait :with_picture do
      after(:create) do |organization|
        FactoryBot.create(:picture, picturable: organization)
      end
    end
    after(:build) do |organization|
      organization.main_name ||= FactoryBot.build(:name, nameable: organization, main: true)
    end
  end
end
