FactoryBot.define do
  factory :link do
    trait :html do
      content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "beethoven.html")) }
      content_extension { ".html" }
    end
    trait :pdf do
      content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "beethoven.pdf")) }
      content_extension { ".pdf" }
    end
    trait :image do
      content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "crucified_barbara.jpg")) }
      content_extension { ".jpg" }
    end
    trait :video do
      content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "pexels_com_seashore-1722595.mp4")) }
      content_extension { ".mp4" }
    end
    trait :audio do
      content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "example.mp3")) }
      content_extension { ".mp3" }
    end
    trait :no_content do
      content_unencrypted { nil }
      content_extension { nil }
      visited_at { nil }
    end
    label { "RandomString-#{SecureRandom.hex}" }
    url { "https://#{SecureRandom.hex}.net" }
    visited_at { DateTime.now }
    content_unencrypted { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    content_extension { ".jpg" }
    linkable { |o| o.association(:person) }
    after(:build) do |link|
      link.signature ||= link.generate_signature
    end
  end
end
