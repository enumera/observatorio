FactoryBot.define do
  factory :user_group do
    name { SecureRandom.hex }
    permissions { {} }
  end
end
