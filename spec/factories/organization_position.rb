FactoryBot.define do
  factory :organization_position do
    name { "Name-#{SecureRandom.uuid}" }
  end
end
