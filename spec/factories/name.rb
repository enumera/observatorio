FactoryBot.define do
  factory :name do
    value { "Name-#{SecureRandom.uuid}" }
    main { false }
    nameable { |o| o.association(:organization) }
  end
end
