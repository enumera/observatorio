FactoryBot.define do
  factory :tag do
    trait :person do
      type { PersonTag }
    end
    trait :organization do
      type { OrganizationTag }
    end
    trait :event do
      type { EventTag }
    end
    name { SecureRandom.hex }
    type { [PersonTag, OrganizationTag, EventTag].sample.name }
  end
end
