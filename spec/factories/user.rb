FactoryBot.define do
  factory :user do
    trait :confirmed do
      confirmed_at { Time.now }
    end
    email { "email@#{SecureRandom.uuid}.com" }
    password { SecureRandom.uuid }
    time_zone { ActiveSupport::TimeZone.all.sample.tzinfo.name }
  end
end
