require_relative "i18n"

ISO3166.configure do |config|
  config.locales = I18n.available_locales.map { |l| l.to_s.sub(/-.*/, "") }
end
