require_relative "boot"

require "rails"
require "active_record/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "action_mailer/railtie"
require "active_job/railtie"
require "action_cable/engine"
require "rails/test_unit/railtie"
require "sprockets/railtie"

APP_CONFIG = YAML.load(ERB.new(File.new("#{File.dirname(__FILE__)}/configuration.yml").read).result)[Rails.env]

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ObservatorioRails5
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Autoload lib folder
    config.eager_load_paths << Rails.root.join("lib")
    # Use Active Job with Sucker Punch backend
    config.active_job.queue_adapter = :sucker_punch
    # Handle exceptions
    config.exceptions_app = self.routes
  end
end
