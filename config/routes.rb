Rails.application.routes.draw do
  # Errors
  get "404", to: "errors#not_found"
  get "500", to: "errors#internal_server_error"
  # Devise
  devise_for :users, path: :account, path_names: { sign_in: "login", sign_out: "logout" }
  devise_scope(:user) { get "logout", to: "devise/sessions#destroy" }
  # Attachments
  resources :attachments do
    member do
      get :download, to: "attachments#download"
      get :history, to: "attachments#history"
    end
  end
  # Educations
  resources :educations
  # Education courses
  resources :education_courses do
    member do
      get :history, to: "education_courses#history"
    end
  end
  # Education degrees
  resources :education_degrees do
    member do
      get :history, to: "education_degrees#history"
    end
  end
  # Events
  resources :events do
    member do
      get   :description,        to: "events#description"
      get   :"description/edit", to: "events#edit_description"
      put   :description,        to: "events#update_description"
      patch :description,        to: "events#update_description"
      get   :history,            to: "events#history"
    end
  end
  resources :event_participants
  # Event Roles
  resources :event_roles do
    member do
      get :history, to: "event_roles#history"
    end
  end
  # Pictures
  resources :pictures
  # Imports
  namespace :imports, only: [:index, :show] do
    resources :memberships, only: [:create, :index, :show] do
      member do
        get :download, to: "memberships#download"
      end
    end
    resources :organizations, only: [:create, :index, :show] do
      member do
        get :download, to: "organizations#download"
      end
    end
  end
  # Links
  resources :links do
    member do
      get :download, to: "links#download"
      get :history, to: "links#history"
    end
  end
  # Locations
  resources :locations, only: [:show] do
    collection do
      post :reverse_geocode, to: "locations#reverse_geocode"
    end
  end
  # Names
  resources :names, except: [:show]
  # Notes
  resources :notes, except: [:edit, :update]
  # Organizations
  resources :organizations do
    member do
      get   :description,        to: "organizations#description"
      get   :"description/edit", to: "organizations#edit_description"
      put   :description,        to: "organizations#update_description"
      patch :description,        to: "organizations#update_description"
      get   :history,            to: "organizations#history"
    end
  end
  # Organization departments
  resources :organization_departments
  # Organiation members
  resources :organization_members
  # Organization owners
  resources :organization_owners
  # Organization positions
  resources :organization_positions do
    member do
      get :history, to: "organization_positions#history"
    end
  end
  # People
  resources :people do
    member do
      get   :biography,        to: "people#biography"
      get   :"biography/edit", to: "people#edit_biography"
      put   :biography,        to: "people#update_biography"
      patch :biography,        to: "people#update_biography"
      get   :history,          to: "people#history"
    end
  end
  # Preferences
  resource :preferences, only: [:edit, :update]
  # Relationships
  resources :relationships
  # Tags
  resources :tags do
    member do
      get :history, to: "tags#history"
    end
  end
  get "/event_tags/:id", to: redirect("/tags/%{id}"), as: "event_tag"
  get "/organization_tags/:id", to: redirect("/tags/%{id}"), as: "organization_tag"
  get "/person_tags/:id", to: redirect("/tags/%{id}"), as: "person_tag"
  # User Groups
  resources :user_groups, only: [:index, :show] do
    member do
      post "users", to: "user_groups#add_user", as: :add_user
      delete "users/:user_id", to: "user_groups#remove_user", as: :remove_user
    end
  end
  # Root
  root to: "home#index"
end
