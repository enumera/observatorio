class OrganizationPosition < ApplicationRecord
  has_name_and_display_priority
  has_paper_trail

  has_many :organization_members, -> { includes(:position, :sources,
                                                :member, :department, :location,
                                                organization: [:headquarters])
                                         .order("people.first_name, people.last_name") }, {
    class_name: "OrganizationMember",
    inverse_of: :position,
    foreign_key: "position_id",
    dependent: :restrict_with_error
  }

  validates_uniqueness_of :name, case_sensitive: false
end
