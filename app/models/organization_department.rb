class OrganizationDepartment < ApplicationRecord
  has_location
  has_name_and_display_priority
  has_paper_trail

  belongs_to :organization
  belongs_to :parent, class_name: "OrganizationDepartment"

  has_many   :members, {
    class_name: "OrganizationMember",
    inverse_of: :department,
    foreign_key: "department_id",
    dependent: :nullify
  }
  has_many   :children, -> {
    includes(children: [
              :location,
              members: [
                :member,
                :organization,
                :location,
                :sources,
                :attachments
                ]
            ])
  }, {
    class_name: "OrganizationDepartment",
    inverse_of: :parent,
    foreign_key: "parent_id",
    dependent: :destroy
  }

  validates_presence_of   :organization
  validates_presence_of   :organization_id
  validates_uniqueness_of :name, scope: [:organization_id], case_sensitive: false

  before_validation :copy_organization_id_from_parent, if: proc { |o|
    o.parent_id? && o.organization_id != o.parent.organization_id
  }

  private

  def copy_organization_id_from_parent
    self.organization_id = parent.organization_id
  end
end
