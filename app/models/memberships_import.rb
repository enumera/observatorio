class MembershipsImport < Import
  private

  def mandatory_columns
    %i(person_first_name person_last_name organization_name)
  end
end
