class OrganizationOwner < ApplicationRecord
  self.table_name = "organizations_owners"

  has_paper_trail
  has_immutable_attributes :organization_id, :owner_id, :owner_type
  has_links :sources
  has_attachments
  has_date_boundaries

  belongs_to :owner, {
    inverse_of: :organizations_owner,
    polymorphic: true
  }
  belongs_to :organization, inverse_of: :owners

  validates_presence_of :owner
  validates_presence_of :organization
  validates :owner_id, {
    uniqueness: { scope: [:owner_type, :organization_id] }
  }
  validates :organization_id, {
    uniqueness: { scope: [:owner_id, :owner_type] }
  }
  validates :portion, {
    presence: true,
    numericality: { greater_than: 0.0, less_than_or_equal_to: 1.0 }
  }
  validate :consistent_portion
  validate :owner_different_from_organization

  def portion_percentage=(value)
    self.portion = value.nil? ? nil : value.to_i.to_f / 100
  end

  private

  def consistent_portion
    return if portion.nil?
    existing_owners = organization.nil? ? [] : organization.owners
    owners = Set.new([self] + existing_owners)
    return if owners.sum(&:portion) <= 1
    errors.add(:portion, organization.errors.generate_message(:base, :invalid_cumulated_portion))
  end

  def owner_different_from_organization
    return if organization != owner
    errors.add(:owner_id, "cannot own itself")
  end
end
