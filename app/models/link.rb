class Link < ApplicationRecord
  default_scope { order(:label) }

  has_paper_trail
  default_value :content_forbidden, false
  default_value :ignore_content, false
  has_immutable_attributes :url, :visited_at, :content, :content_size,
                           :content_extension, :content_encryption_iv_base64
  has_file :content, mandatory: false

  strip :label

  belongs_to :linkable, polymorphic: true

  validates_presence_of   :url
  validates_presence_of   :label
  validates_presence_of   :visited_at
  validates_presence_of   :signature, unless: :ignore_content?
  validates_uniqueness_of :label, {
    scope: [:linkable_id, :linkable_type],
    case_sensitive: false
  }
  validates_uniqueness_of :url, {
    scope: [:linkable_id, :linkable_type],
    case_sensitive: false
  }
  validate :content_exists
  validate :validate_signature

  before_validation :download_content
  after_create :create_link_inverse_relationship, {
    if: proc { |o| o.linkable.is_a?(Relationship) }
  }
  after_update :update_link_inverse_relationship, {
    if: proc { |o| o.saved_changes? && o.linkable.is_a?(Relationship) }
  }
  after_destroy :destroy_link_inverse_relationship, {
    if: proc { |o| o.linkable.is_a?(Relationship) }
  }

  def generate_signature
    return nil unless visited_at.present?
    Digest::SHA512.base64digest(
      "#{url}|#{visited_at.to_i}|" \
      "#{content_extension}|#{content_forbidden}|" \
      "#{Rails.application.secrets.secret_key_base}|" \
      "#{content_decrypted&.read}"
    )
  end

  private

  def validate_signature
    return if ignore_content? || !url_changed?
    return if signature == generate_signature
    errors.add(:signature, I18n.t("errors.messages.invalid"))
  end

  def download_content
    return unless content_required?
    content_head = DOWNLOADER.head(url)
    self.content_extension = content_head.extension
    if url_youtube? || url_vimeo?
      self.visited_at = content_head.visited_at
      self.content_forbidden = true
    else
      downloaded = content_html? ? HTML_TO_PDF.convert(url) : DOWNLOADER.get(url)
      self.visited_at = downloaded.visited_at
      self.content_unencrypted = downloaded.file
      self.content_extension = downloaded.extension
    end
  rescue Downloader::Forbidden,
         Downloader::Error,
         HtmlToPdf::Error => e
    self.visited_at = Time.now
    self.content_forbidden = true
  rescue Downloader::NotFound
  end

  def url_youtube?
    return false unless url.present?
    host = URI.parse(url).host
    host == "youtu.be" ||
      host.include?(".youtube.") ||
      host.start_with?("youtube.")
  end

  def url_vimeo?
    return false unless url.present?
    host = URI.parse(url).host
    host.include?(".vimeo.") || host.start_with?("vimeo.")
  end


  def content_exists
    return unless content_required?
    errors.add(:url, I18n.t("errors.messages.invalid"))
  end

  def content_required?
    !content.present? && !content_forbidden? && !ignore_content?
  end

  def create_link_inverse_relationship
    inverse_relationship = linkable.inverse_relationship
    return if Link.find_by(linkable: inverse_relationship, label: label).present?
    Link.create!(
      url: url,
      label: label,
      linkable: inverse_relationship,
      visited_at: visited_at,
      content: content,
      content_size: content_size,
      content_extension: content_extension,
      content_encryption_iv_base64: content_encryption_iv_base64,
      signature: signature
    )
  end

  def update_link_inverse_relationship
    old_label = saved_changes["label"].first
    inverse_link = Link.find_by(
      label: old_label,
      linkable: linkable.inverse_relationship
    )
    inverse_link.update_attributes!(label: label) if inverse_link.present?
  end

  def destroy_link_inverse_relationship
    inverse_link = Link.find_by(
      label: label, linkable: linkable.inverse_relationship
    )
    inverse_link.destroy! if inverse_link.present?
  end
end
