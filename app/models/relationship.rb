class Relationship < ApplicationRecord
  DEGREES = %w(child parent partner sibling)
  DEGREES_ETERNAL = %w(child parent sibling)

  has_paper_trail
  has_links :sources
  has_attachments
  has_date_boundaries
  has_immutable_attributes :owner_id, :with_id

  belongs_to :owner, class_name: "Person"
  belongs_to :with,  class_name: "Person"

  validates_presence_of   :owner
  validates_presence_of   :with
  validates               :owner_id, uniqueness: { scope: [:with_id] }
  validates               :with_id, uniqueness: { scope: [:owner_id] }
  validates_inclusion_of  :degree, in: DEGREES

  before_validation :clear_date_boundaries, if: :eternal?
  after_create :create_inverse_relationship
  after_update :update_inverse_relationship, if: :saved_changes?
  after_destroy :destroy_inverse_relationship

  def eternal?
    DEGREES_ETERNAL.include?(degree)
  end

  def inverse_relationship_degree
    case degree
    when "parent"
      "child"
    when "child"
      "parent"
    else
      degree
    end
  end

  def inverse_relationship
    Relationship.find_by(owner_id: with_id, with_id: owner_id)
  end

  private

  def clear_date_boundaries
    self.from_date = nil
    self.to_date = nil
    self.approximate_dates = nil
  end

  def destroy_inverse_relationship
    inverse = inverse_relationship
    inverse.present? && inverse.destroy!
  end

  def create_inverse_relationship
    return if inverse_relationship.present?
    Relationship.create!(inverse_relationship_attributes)
  end

  def update_inverse_relationship
    inverse_relationship.update_attributes!(inverse_relationship_attributes)
  end

  def inverse_relationship_attributes
    {
      owner: with,
      with: owner,
      from_date: from_date,
      to_date: to_date,
      approximate_dates: approximate_dates,
      degree: inverse_relationship_degree
    }
  end
end
