class Person < ApplicationRecord
  has_paper_trail
  has_picture
  has_links
  has_attachments
  has_notes
  has_names
  has_date_boundaries from_date: :date_of_birth, to_date: :date_of_death
  has_location :birth_location
  has_location :death_location
  default_value :nationalities, []

  has_many :organizations_member, {
    class_name: "OrganizationMember",
    inverse_of: :member,
    foreign_key: "member_id",
    dependent: :destroy
  }
  has_many  :organizations_owner, {
    class_name: "OrganizationOwner",
    inverse_of: :owner,
    as: :owner,
    foreign_key: "owner_id",
    dependent: :destroy
  }
  has_many :events_participant, {
    class_name: "EventParticipant",
    inverse_of: :participant,
    as: :participant,
    foreign_key: "participant_id",
    dependent: :destroy
  }
  has_many :relationships, {
    class_name: "Relationship",
    inverse_of: :owner,
    foreign_key: "owner_id",
    dependent: :destroy
  }
  has_many :inverse_relationships, {
    class_name: "Relationship",
    inverse_of: :with,
    foreign_key: "with_id",
    dependent: :destroy
  }
  has_many :educations, {
    inverse_of: :student,
    foreign_key: "student_id",
    dependent: :destroy
  }
  has_many :tagged_people, {
    class_name: "TaggedPerson",
    inverse_of: :person,
    foreign_key: "person_id"
  }
  has_many :tags, through: :tagged_people, dependent: :destroy

  validate :nationalities_iso3166

  before_validation :compact_nationalities
  before_validation :remove_duplicate_nationalities
  before_validation :sort_nationalities

  private

  def nationalities_iso3166
    return if (nationalities - Country.all.map(&:alpha2)).empty?
    errors.add(:nationalities, I18n.t("errors.invalid_format"))
  end

  def compact_nationalities
    nationalities.reject!(&:blank?)
  end

  def remove_duplicate_nationalities
    nationalities.uniq!
  end

  def sort_nationalities
    nationalities.sort!
  end
end
