module HasNotes
  extend ActiveSupport::Concern

  class_methods do
    def has_notes
      has_many :notes, {
        as: :subject,
        dependent: :destroy
      }
    end
  end
end
