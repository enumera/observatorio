function initEventParticipantModal(modal) {
  // Configure datetimes
  var fromDateTimeInput = $('input[name="event_participant[from_datetime]"]', modal);
  var toDateTimeInput = $('input[name="event_participant[to_datetime]"]', modal);
  function refreshFromDateTime() {
    var timeZone = $('select[name="event_participant[time_zone]"]', modal).val() || 'Etc/UTC';
    var fromDateTimeDate = $('input[name="event_participant[from_datetime_date]"]', modal).val();
    if (fromDateTimeDate) {
      var fromDateTimeTime = $('input[name="event_participant[from_datetime_time]"]', modal).val();
      fromDateTimeInput.val(moment.tz(fromDateTimeDate + (fromDateTimeTime ? ' ' + fromDateTimeTime : ''), timeZone).format());
    } else {
      fromDateTimeInput.val('');
    }
  }
  function refreshToDateTime() {
    var timeZone = $('select[name="event_participant[time_zone]"]', modal).val() || 'Etc/UTC';
    var toDateTimeDate = $('input[name="event_participant[to_datetime_date]"]', modal).val();
    if (toDateTimeDate) {
      var toDateTimeTime = $('input[name="event_participant[to_datetime_time]"]', modal).val();
      toDateTimeInput.val(moment.tz(toDateTimeDate + (toDateTimeTime ? ' ' + toDateTimeTime : ''), timeZone).format());
    } else {
      toDateTimeInput.val('');
    }
  }

  $('input[name="event_participant[from_datetime_date]"],' +
    'input[name="event_participant[from_datetime_time]"],' +
    'input[name="event_participant[to_datetime_date]"],' +
    'input[name="event_participant[to_datetime_time]"],' +
    'select[name="event_participant[time_zone]"]', modal).change(function(event) {
    refreshFromDateTime();
    refreshToDateTime();
  });

  var eventSelect = $('select[name="event_participant[event_id]"]', modal);
  eventSelect.change(function() {
    var event = eventSelect.find(':selected');
    if (event.attr('data-time-zone')) {
      $('select[name="event_participant[time_zone]"]', modal).val(event.attr('data-time-zone'));
      if (event.attr('data-started-at')) {
        var fromDateTimeTz = moment.tz(event.attr('data-started-at'), event.attr('data-time-zone'));
        $('input[name="event_participant[from_datetime_date]"]', modal).val(fromDateTimeTz.format('YYYY-MM-DD'));
        $('input[name="event_participant[from_datetime_time]"]', modal).val(fromDateTimeTz.format('HH:mm'));
        refreshFromDateTime();
      } else {
        $('input[name="event_participant[from_datetime_date]"]', modal).val('');
        $('input[name="event_participant[from_datetime_time]"]', modal).val('');
      }
      if (event.attr('data-finished-at')) {
        var toDateTimeTz = moment.tz(event.attr('data-finished-at'), event.attr('data-time-zone'));
        $('input[name="event_participant[to_datetime_date]"]', modal).val(toDateTimeTz.format('YYYY-MM-DD'));
        $('input[name="event_participant[to_datetime_time]"]', modal).val(toDateTimeTz.format('HH:mm'));
        refreshToDateTime();
      } else {
        $('input[name="event_participant[to_datetime_date]"]', modal).val('');
        $('input[name="event_participant[to_datetime_time]"]', modal).val('');
      }
    }
  });

  var participantSelect = $('select[name$="[participant_id]"]', modal);
  participantSelect.change(function() {
    var selected = participantSelect.find(':selected');
    $('input[name$="[participant_type]"]', modal).val(selected.attr('data-type'));
  });
}
