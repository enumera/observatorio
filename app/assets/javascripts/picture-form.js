function initPictureForm(modal) {
  var fileGroup = $('.file-group', modal);
  var urlGroup = $('.url-group', modal);
  var sourceGroup = $('.source-group', modal);
  var authorGroup = $('.author-group', modal);
  var licenseGroup = $('.license-group', modal);
  $('input[name="pictureType"]', modal).change(function(event) {
    var input = $(event.target);
    if (input.val() === 'file') {
      urlGroup.hide();
      $('input', urlGroup).prop('disabled', true);
      fileGroup.show();
      sourceGroup.show();
      $('input', sourceGroup).prop('disabled', false);
      authorGroup.show();
      licenseGroup.show();
    } else if (input.val() === 'url') {
      fileGroup.hide();
      sourceGroup.hide();
      $('input', sourceGroup).prop('disabled', true);
      urlGroup.show();
      $('input', urlGroup).prop('disabled', false);
      authorGroup.show();
      licenseGroup.show();
      $('input', urlGroup).focus();
    }
  });
}
