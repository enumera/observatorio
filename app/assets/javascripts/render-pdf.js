var pageRendering = false;
var pageNumPending = null;
var pdfDoc = null;
var pageNum = 0;
var pdfCanvas = null;

function renderNextPage() {
  pageNum = pageNum + 1;
  renderPage(pageNum);
  return pageNum == pdfDoc.numPages;
}

function renderPreviousPage() {
  pageNum = pageNum - 1;
  renderPage(pageNum);
  return pageNum == 1;
}

function renderPage(pageNum) {
  if (pageRendering) {
    pageNumPending = pageNum;
    return;
  }
  pageRendering = true;
  pdfDoc.getPage(pageNum).then(function(page) {
    var viewport = page.getViewport(2);
    // Prepare canvas using PDF page dimensions
    var context = pdfCanvas.getContext('2d');
    pdfCanvas.height = viewport.height;
    pdfCanvas.width = viewport.width;
    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending) {
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });
}

function renderPdf(canvas, options, pageNum) {
  pdfCanvas = canvas;
  // Loaded via <script> tag, create shortcut to access PDF.js exports.
  var pdfjsLib = window['pdfjs-dist/build/pdf'];
  // The workerSrc property shall be specified.
  pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.worker.min.js';
  // Load document
  var loadingTask = pdfjsLib.getDocument(options);
  loadingTask.promise.then(function(pdf) {
    pdfDoc = pdf;
    renderNextPage();
  });
  return loadingTask;
}
