function GeoMap(container) {
  function onMapLoad() {
    this.mapbox.addSource('circle-data', { type: 'geojson', data: this.emptyGeojson() });
    this.mapbox.addSource('line-data', { type: 'geojson', data: this.emptyGeojson() });
    this.mapbox.addSource('fill-data', { type: 'geojson', data: this.emptyGeojson() });
    this.mapbox.addLayer({
      "id": "circle",
      "source": "circle-data",
      "type": "circle",
      "paint": {
        "circle-color": "#3887be",
        "circle-radius": 6,
        "circle-opacity": 0.75
      }
    });
    this.mapbox.addLayer({
      "id": "line",
      "source": "line-data",
      "type": "line",
      "paint": {
        "line-color": "#3887be",
        "line-width": 8, "line-opacity": 0.75
      }
    });
    this.mapbox.addLayer({
      "id": "fill",
      "source": "fill-data",
      "type": "fill",
      "paint": {
        "fill-color": "#3887be",
        "fill-opacity": 0.75
      }
    });
  }
  this.container = container;
  this.container.show();
  this.mapbox = new mapboxgl.Map({
    container: container.attr('id'),
    style: 'mapbox://styles/mapbox/streets-v9',
    zoom: 1
  });
  this.mapbox.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
  this.mapbox.addControl(new MapboxLanguage({
    supportedLanguages: [currentLanguage()],
    defaultLanguage: currentLanguage()
  }));
  this.mapbox.on('load', onMapLoad.bind(this));
}

GeoMap.prototype.addSearch = function(form, targetInput) {
  function onResultGeocoder(event) {
    function onSuccessCreateLocation(json) {
      this.displayGeometry(json.geojson);
      this.enableFormSubmit();
      this.enableSearch();
      this.setCursor('crosshair');
      this.input.val(json.id);
    }
    function onErrorCreateLocation() {
      this.enableFormSubmit();
      this.enableSearch();
    }
    var result = event.result;
    this.mapbox.getSource('line-data').setData(this.emptyGeojson());
    this.mapbox.getSource('fill-data').setData(this.emptyGeojson());
    this.mapbox.getSource('circle-data').setData(this.emptyGeojson());
    this.disableFormSubmit();
    this.disableSearch();
    this.setCursor('wait');
    params = {
      location: {
        osm_id: result.osm_id,
        osm_type: result.osm_type
      }
    };
    if (result.osm_type === 'node') {
      params.location.lat = result.lat;
      params.location.lng = result.lon;
    }
    $.ajax({
      url: this.input.attr('data-locations-path'),
      method: 'POST',
      data: params,
      success: onSuccessCreateLocation.bind(this),
      error: onErrorCreateLocation.bind(this)
    });
  }
  function onMouseDown() {
    function onMoveCancelClick() {
      if (!this.clickOnMap) {
        return;
      }
      this.setCursor('move');
      this.clickOnMap = false;
      this.mapbox.off('mousemove', this.onMoveCancelClick);
    }
    function onMouseUpClickOnSelect(event) {
      function onSuccessLocationSelect(json) {
        this.mapbox.getSource('circle-data').setData(json.geojson);
        this.enableSearch();
        this.setSearch(json.formatted_address);
        this.searchLoading = false;
        this.enableFormSubmit();
        this.setCursor('crosshair');
        this.mapbox.dragPan.enable();
        this.input.val(json.id);
      }
      function onErrorLocationSelect() {
        this.enableSearch();
        this.searchLoading = false;
        this.enableFormSubmit();
        this.setCursor('crosshair');
        this.mapbox.dragPan.enable();
      }
      this.setCursor('crosshair');
      if (!this.clickOnMap) {
        return;
      }
      this.geocoder._clear();
      this.mapbox.getSource('line-data').setData(this.emptyGeojson());
      this.mapbox.getSource('fill-data').setData(this.emptyGeojson());
      this.mapbox.getSource('circle-data').setData(this.emptyGeojson());
      this.clickOnMap = false;
      this.setSearch(null);
      this.disableSearch();
      this.searchLoading = true;
      this.disableFormSubmit();
      this.mapbox.off('mousemove', onMoveCancelClick.bind(this));
      this.setCursor('wait');
      this.mapbox.dragPan.disable();
      $.ajax({
        url: this.input.attr('data-locations-path'),
        method: 'POST',
        data: {
          location: {
            lat: event.lngLat.lat,
            lng: event.lngLat.lng
          }
        },
        success: onSuccessLocationSelect.bind(this),
        error: onErrorLocationSelect.bind(this)
      });
    }
    if (this.searchLoading) {
      return;
    }
    this.clickOnMap = true;
    this.mapbox.on('mousemove', onMoveCancelClick.bind(this));
    this.mapbox.once('mouseup', onMouseUpClickOnSelect.bind(this));
  }
  this.form = form;
  this.input = targetInput;
  this.setCursor('crosshair');
  this.geocoder = new MapboxOsmGeocoder({
    accessToken: mapboxgl.accessToken,
    language: currentLocale(),
    placeholder: this.input.attr('data-geocoding-placeholder')
  });
  this.geocoder.on('result', onResultGeocoder.bind(this));
  this.mapbox.addControl(this.geocoder, 'top-left');
  this.clickOnMap = false;
  this.searchLoading = false;
  this.mapbox.on('mousedown', onMouseDown.bind(this));
};

GeoMap.prototype.displayGeometry = function(geometry) {
  if (geometry['type'] === 'Point') {
    this.mapbox.getSource('line-data').setData(this.emptyGeojson());
    this.mapbox.getSource('fill-data').setData(this.emptyGeojson());
    this.mapbox.getSource('circle-data').setData(geometry);
    this.mapbox.flyTo({ center: geometry.coordinates, zoom: 14 });
  }
  if (geometry['type'] === 'LineString') {
    this.mapbox.getSource('circle-data').setData(this.emptyGeojson());
    this.mapbox.getSource('fill-data').setData(this.emptyGeojson());
    this.mapbox.getSource('line-data').setData(geometry);
  }
  if (geometry['type'] === 'Polygon' || geometry['type'] === 'MultiPolygon') {
    this.mapbox.getSource('circle-data').setData(this.emptyGeojson());
    this.mapbox.getSource('line-data').setData(this.emptyGeojson());
    this.mapbox.getSource('fill-data').setData(geometry);
  }
};

GeoMap.prototype.disableScrollZoom = function() {
  this.mapbox.scrollZoom.disable();
};

GeoMap.prototype.disableDoubleClickZoom = function() {
  this.mapbox.doubleClickZoom.disable();
};

GeoMap.prototype.enableFormSubmit = function() {
  $('input[type="submit"]', this.form).prop('disabled', false);
};

GeoMap.prototype.disableFormSubmit = function() {
  $('input[type="submit"]', this.form).prop('disabled', true);
};

GeoMap.prototype.enableSearch = function() {
  $('.mapboxgl-ctrl-geocoder input', this.container).prop('disabled', false);
};

GeoMap.prototype.disableSearch = function() {
  $('.mapboxgl-ctrl-geocoder input', this.container).prop('disabled', true);
};

GeoMap.prototype.setSearch = function(value) {
  $('.mapboxgl-ctrl-geocoder input', this.container).val(value);
  if (value) {
    $('.mapboxgl-ctrl-geocoder .geocoder-icon-close', this.container).show();
  } else {
    $('.mapboxgl-ctrl-geocoder .geocoder-icon-close', this.container).hide();
  }
};

GeoMap.prototype.setCursor = function(cursor) {
  this.mapbox.getCanvasContainer().style.cursor = cursor;
};

GeoMap.prototype.setBounds = function(bbox) {
  this.mapbox.fitBounds(bbox, {
    padding: { top: 100, bottom: 100, left: 100, right: 100 }
  });
}

GeoMap.prototype.setPosition = function(lat, lng) {
  this.mapbox.flyTo({ center: [lng, lat], zoom: 14 })
}

GeoMap.prototype.emptyGeojson = function() {
  return {
    "type": "FeatureCollection",
    "features": []
  };
};
