// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require_tree .
//

function initAll(root) {
  /*
  * Setup tooltips
  */
  $('.has-tooltip', root).tooltip({ trigger: 'hover' });
  /*
  * Setup maps
  */
  $('.map-modal', root).on('shown.bs.modal', function(event) {
    var modal = $(event.target);
    var mapContainer = $('.map-container', modal);
    $.ajax({
      url: mapContainer.attr('data-location-path'),
      dataType: 'json',
      success: function(json) {
        var geomap = new GeoMap(mapContainer);
        geomap.mapbox.on('load', function() {
          if (json.boundingbox) {
            geomap.setBounds([
              [json.boundingbox[2], json.boundingbox[0]],
              [json.boundingbox[3], json.boundingbox[1]]
            ]);
          }
          if (json.lat && json.lng) {
            geomap.setPosition(json.lat, json.lng);
          }
          geomap.displayGeometry(json.geojson);
        });
      }
    });
  });
  $('.map-modal', root).on('hidden.bs.modal', function(event) {
    var modal = $(event.target);
    var mapContainer = $('.map-container', modal);
    mapContainer.empty();
  });
  /*
  * Setup modal forms
  */
  $('.modal-form', root).on('shown.bs.modal', function(event) {
    modal = $(event.target);
    getForm(modal);
  });
  $('.modal-form', root).on('hidden.bs.modal', function(event) {
    modal = $(event.target)
    var errorDiv = $('.modal-form-error', modal);
    var contentDiv = $('.modal-form-content', modal);
    var loadingDiv = $('.loading', modal);
    errorDiv.hide();
    contentDiv.hide();
    loadingDiv.show();
  });
  $('.modal-form-static', root).each(function(index, element) {
    var modal = $(element);
    var form = $('form', modal);
    var loadingDiv = $('.loading', modal);
    loadingDiv.hide();
    form.submit(submitForm.bind(null, modal));
  });
  /*
  * Setup collapse actions
  */
  $('.action-collapse', root).each(function(index, element) {
    var action = $(element);
    action.click(function(event) {
      event.preventDefault();
      if(action.hasClass('shown')) {
        action.removeClass('shown');
      } else {
        action.addClass('shown');
      }
      $(action.attr('href')).collapse('toggle');
    });
  });
}

$(document).ready(function() {
  initAll(document);
  /*
  * Render PDFs
  */
  var pdfContainer = $('.pdf-container');
  var pdfNext = $('.pdf-next');
  var pdfPrevious = $('.pdf-previous');
  if (pdfContainer.attr('data-pdf-url')) {
    var loadingTask = renderPdf(pdfContainer[0], pdfContainer.attr('data-pdf-url'));
    loadingTask.promise.then(function(pdf) {
      if (pdf.numPages > 1) {
        pdfNext.removeClass('disabled');
      }
    });
  }
  pdfNext.click(function(event) {
    event.preventDefault();
    var button = $(event.target);
    if (button.hasClass('disabled')) {
      return;
    }
    var lastPage = renderNextPage();
    if (lastPage) {
      pdfNext.addClass('disabled');
    }
    pdfPrevious.removeClass('disabled');
  });
  pdfPrevious.click(function(event) {
    event.preventDefault();
    var button = $(event.target);
    if (button.hasClass('disabled')) {
      return;
    }
    var firstPage = renderPreviousPage();
    if (firstPage) {
      pdfPrevious.addClass('disabled');
    }
    pdfNext.removeClass('disabled');
  });
  /*
  * Setup lazyload videos
  */
  $('.lazyload-video').each(function(_, element) {
    var videoContainer = $(element);
    var video = $('video', element);
    var videoSource = $('source', video);
    var videoPlaceholder = $('.lazyload-video-placeholder')
    var loading = $('.loading', videoPlaceholder);
    var playButtonImage = $('.svg', videoPlaceholder);
    videoPlaceholder.click(function() {
      playButtonImage.hide();
      loading.show();
      setTimeout(function() {
        videoSource.attr('src', videoSource.attr('data-src'));
        video[0].load();
        video.on('canplay', function() {
          videoPlaceholder.hide();
          video.show();
          video[0].play();
        });
      }, 500)
    });
  });
  /*
  * Setup refreshables
  */
  $(".refreshable").each(function(_, element) {
    var refreshable = $(element);
    refresh(refreshable);
  });
  /*
  * Setup ajax authenticity_token
  */
  $.ajaxSetup({
    data: {
      authenticity_token: $("meta[name=csrf-token]").attr("content")
    }
  });
});
