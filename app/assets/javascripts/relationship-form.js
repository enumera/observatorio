function configureRelationshipDateBoundaries(modal) {
  var boundariesGroups = $('.date-boundaries-group', modal);
  var degreeOption = $('.degree-group select', modal).find(':selected');
  if (degreeOption[0].hasAttribute('data-eternal')) {
    $('input', boundariesGroups).prop('disabled', true);
    boundariesGroups.hide();
  } else {
    $('input', boundariesGroups).prop('disabled', false);
    boundariesGroups.show();
  }
}

function initRelationshipForm(modal) {
  configureRelationshipDateBoundaries(modal);
  $('.degree-group select', modal).change(function() {
    configureRelationshipDateBoundaries(modal);
  });
}
