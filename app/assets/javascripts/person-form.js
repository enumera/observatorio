function initPersonForm(modal) {
  // Init nationalities
  var form = $('form', modal);
  var modalBody = $('.modal-body', modal);
  var nationalitiesList = $('.nationalities-list', form);
  var nationalityTemplate = $('.nationality-template', form);
  var addNationality = $('#add-nationality', form);
  nationalityTemplate.hide();
  addNationality.click(function(event) {
    event.preventDefault();
    var newNationality = nationalityTemplate.clone();
    newNationality.removeClass('nationality-template');
    $('input, select', newNationality).removeAttr('disabled');
    addNationality.before(newNationality);
    $('a', newNationality).click(function(event) {
      event.preventDefault();
      newNationality.remove();
    });
    newNationality.show();
    modalBody.animate({ scrollTop: addNationality.position().top + modalBody.scrollTop() }, 'slow');
    $('select', newNationality).focus();
  });
  var currentNationalities = $('.form-group:not(.nationality-template)', nationalitiesList);
  currentNationalities.each(function(index, element) {
    nationality = $(element);
    $('a', nationality).click(function(event) {
      event.preventDefault();
      nationality.remove();
    });
  });
}
