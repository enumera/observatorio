$(document).ready(function() {
  var showAttachments = $('.attachments.show');
  var attachmentContent = $('.attachment-content', showAttachments);
  var loadingContent = $('.loading-attachment', showAttachments);
  var label = $('.attachment-label', attachmentContent);
  var description = $('.attachment-description', attachmentContent);
  $('.modal-form', showAttachments).on('hidden.bs.modal', function(event) {
    var modal = $(event.target);
    var refreshAttachment = $('input[name="refresh_element"][value="#history-subsection"]', modal);
    if (refreshAttachment.length) {
      attachmentContent.css('visibility', 'hidden');
      loadingContent.show();
      $.ajax({
        url: attachmentContent.attr('data-url'),
        dataType: 'json',
        success: function(json) {
          setTimeout(function() {
            if (json.description) {
              description.html(json.description.replaceAll('\n', '</br>'));
            } else {
              description.empty();
            }
            label.html(json.label);
            loadingContent.hide();
            attachmentContent.css('visibility', 'visible');
          }, 400);
        }
      });
    }
  });
});
