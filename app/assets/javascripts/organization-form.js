function initOrganizationModal(modal) {
  var cnpjInput = $('input[name="organization[cnpj]"]', modal);
  cnpjInput.mask('00.000.000/0000-00');
}
