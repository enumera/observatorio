class PreferencesController < ApplicationController
  skip_authorization_check

  # GET /preferences/edit
  def edit
    @resource = current_user
  end

  # PUT /preferences
  def update
    @resource = current_user
    @resource.assign_attributes(update_params)
    if @resource.save
      flash[:success] = I18n.t("observatorio.preferences.updated")
      redirect_to root_path
    else
      render :edit
    end
  end

  private

  def update_params
    params.require(:user)
          .permit(:time_zone)
  end
end
