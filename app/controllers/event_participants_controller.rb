class EventParticipantsController < ResourcesController
  layout false

  # GET /event_participants
  def index
    super
    @resources = @resources
                   .includes(:event,
                             :role,
                             :location,
                             :sources,
                             :attachments)
                   .order("events.name")
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == EventParticipant
      participant_id = params.require(:event_participant)[:participant_id]
      if participant_id.present?
        participant_klass = params.require(:event_participant)
                                  .require(:participant_type)
                                  .constantize
        participant = participant_klass.find(participant_id)
        authorize! :read, participant
      else
        event_id = params.require(:event_participant)
                         .require(:event_id)
        event = Event.find(event_id)
        authorize! :read, event
      end
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    EventParticipant
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def create_params
    params.require(:event_participant)
          .permit(:event_id,
                  :participant_id,
                  :participant_type,
                  :role_id,
                  :from_datetime,
                  :to_datetime,
                  :approximate_dates,
                  :location_id)
  end

  def update_params
    params.require(:event_participant)
          .permit(:role_id,
                  :from_datetime,
                  :to_datetime,
                  :approximate_dates,
                  :location_id)
  end

  def index_or_new_params
    params.require(:event_participant)
          .permit(:event_id,
                  :participant_id,
                  :participant_type)
  end
end
