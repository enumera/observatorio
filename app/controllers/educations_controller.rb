class EducationsController < ResourcesController
  layout false

  # GET /educations
  def index
    super
    @resources = @resources
                  .includes(:student,
                            :school,
                            :course,
                            :degree,
                            :sources,
                            :attachments)
                  .order(:from_date, :to_date)
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Education
      student_id = params.require(:education)[:student_id]
      if student_id.present?
        student = Person.find(student_id)
        authorize! :read, student
      else
        school_id = params.require(:education).require(:school_id)
        school = Organization.find(school_id)
        authorize! :read, school
      end
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    Education
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:education)
          .permit(:school_id,
                  :student_id)
  end

  def create_params
    params.require(:education)
          .permit(:school_id,
                  :student_id,
                  :course_id,
                  :degree_id,
                  :from_date,
                  :to_date,
                  :approximate_dates)
  end

  def update_params
    params.require(:education)
          .permit(:course_id,
                  :degree_id,
                  :from_date,
                  :to_date,
                  :approximate_dates)
  end
end
