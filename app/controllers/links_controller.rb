class LinksController < ResourcesController
  # GET /links
  def index
    super
    @resources = @resources.includes(:linkable)
    render :index, layout: false
  end

  # GET /links/:id
  def show
    super
    respond_to do |format|
      format.html { render :show }
      format.json { render :show }
    end
  end

  # GET /links/:id/download
  def download
    link = Link.find(params.require(:id))
    authorize! :read, link
    if link.content_html?
      html_document = Nokogiri::HTML(link.content_decrypted)
      uri = URI.parse(link.url)
      base_url = "#{uri.scheme}://#{uri.host}"
      # Remove scripts
      html_document.css("script").each(&:remove)
      # Replace relative URLs in inline CSS
      html_document.css("style").each do |style_tag|
        style_tag.content = style_tag.content.gsub("url('/", "url('#{base_url}/")
      end
      # Add partial
      iframe_content = Nokogiri::HTML::DocumentFragment.parse(
        render_to_string(partial: "links/iframe_content",
                         locals: { base_url: base_url })
      )
      (html_document.at_css("head") || html_document.root).prepend_child(iframe_content)
      tempfile = Tempfile.open(["", link.content_extension])
      tempfile.write(html_document.to_html)
      tempfile.rewind
      send_file(tempfile.path, disposition: "inline")
    else
      send_file(link.content_decrypted.path,
                filename: "#{link.label}-#{link.id}#{link.content_extension}")
    end
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Link
      linkable_id = params.require(:link).require(:linkable_id)
      linkable_type = params.require(:link).require(:linkable_type)
      authorize! :read, linkable_type.constantize.find(linkable_id)
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    Link
  end

  def versions_unsorted
    resource_versions
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:link)
          .permit(:linkable_id,
                  :linkable_type)
  end

  def create_params
    params.require(:link)
          .permit(:url,
                  :label,
                  :signature,
                  :visited_at,
                  :description,
                  :linkable_id,
                  :linkable_type,
                  :ignore_content,
                  :content_forbidden,
                  :content_extension,
                  :content_unencrypted_base64)
  end

  def update_params
    params.require(:link)
          .permit(:label,
                  :description)
  end
end
