class NamesController < ResourcesController
  layout false

  def index
    super
    @resources = @resources.order(:value)
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Name
      nameable_id = params.require(:name).require(:nameable_id)
      nameable_type = params.require(:name).require(:nameable_type)
      authorize! :read, nameable_type.constantize.find(nameable_id)
    else
      super
    end
  end

  def resources_class
    Name
  end

  def resources_per_page
    nil
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:name)
          .permit(:main,
                  :nameable_id,
                  :nameable_type)
  end

  def create_params
    params.require(:name)
          .permit(:value,
                  :nameable_id,
                  :nameable_type)
  end

  def update_params
    params.require(:name)
          .permit(:value)
  end
end
