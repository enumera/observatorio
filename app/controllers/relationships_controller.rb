class RelationshipsController < ResourcesController
  layout false

  # GET /relationships
  def index
    super
    @resources = @resources
                  .includes(:sources,
                            :attachments,
                            with: [:names])
                  .order("names.value")
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == Relationship
      owner = Person.find(params.require(:relationship).require(:owner_id))
      authorize! :read, owner
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    Relationship
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:relationship)
          .permit(:owner_id)
  end

  def create_params
    params.require(:relationship)
          .permit(:owner_id,
                  :with_id,
                  :degree,
                  :from_date,
                  :to_date,
                  :approximate_dates)
  end

  def update_params
    params.require(:relationship)
          .permit(:degree,
                  :from_date,
                  :to_date,
                  :approximate_dates)
  end
end
