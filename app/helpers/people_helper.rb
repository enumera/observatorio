module PeopleHelper
  def countries_options
    sorted_countries = Country.all.sort_by do |country|
      country.translation(I18n.locale)
    end
    sorted_countries.map do |country|
      ["#{country.translation(I18n.locale)} #{country.emoji_flag}", country.alpha2]
    end
  end

  def display_country(alpha2)
    country = Country[alpha2]
    tag.span(country.emoji_flag,  {
      class: "has-tooltip nationality",
      title: country.translation(I18n.locale)
    })
  end

  def display_country_proc
    proc { |alpha2| Country[alpha2].translation(I18n.locale) }
  end
end
