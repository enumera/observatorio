module OpenIconicHelper
  def download(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0v3h-2l3 3 3-3h-2v-3h-2zm-3 7v1h8v-1h-8z' />
    </svg>".html_safe
  end

  def pencil(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M6 0l-1 1 2 2 1-1-2-2zm-2 2l-4 4v2h2l4-4-2-2z' />
    </svg>".html_safe
  end

  def x(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M1.41 0l-1.41 1.41.72.72 1.78 1.81-1.78 1.78-.72.69 1.41 1.44.72-.72 1.81-1.81 1.78 1.81.69.72 1.44-1.44-.72-.69-1.81-1.78 1.81-1.81.72-.72-1.44-1.41-.69.72-1.78 1.78-1.81-1.78-.72-.72z' />
    </svg>".html_safe
  end

  def check(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M6.41 0l-.69.72-2.78 2.78-.81-.78-.72-.72-1.41 1.41.72.72 1.5 1.5.69.72.72-.72 3.5-3.5.72-.72-1.44-1.41z' transform='translate(0 1)' />
    </svg>".html_safe
  end

  def plus(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0v3h-3v2h3v3h2v-3h3v-2h-3v-3h-2z' />
    </svg>".html_safe
  end

  def info(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1zm-1.5 2.5c-.83 0-1.5.67-1.5 1.5h1c0-.28.22-.5.5-.5s.5.22.5.5-1 1.64-1 2.5c0 .86.67 1.5 1.5 1.5s1.5-.67 1.5-1.5h-1c0 .28-.22.5-.5.5s-.5-.22-.5-.5c0-.36 1-1.84 1-2.5 0-.81-.67-1.5-1.5-1.5z' transform='translate(2)'
      />
    </svg>".html_safe
  end

  def minus(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M0 0v2h8v-2h-8z' transform='translate(0 3)' />
    </svg>".html_safe
  end

  def download(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0v3h-2l3 3 3-3h-2v-3h-2zm-3 7v1h8v-1h-8z' />
    </svg>".html_safe
  end

  def circle(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z' transform='translate(1 1)' />
    </svg>".html_safe
  end

  def ellipses(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M0 0v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2z' transform='translate(0 3)' />
    </svg>".html_safe
  end

  def arrow_left(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M3 0l-3 2.53 3 2.47v-2h5v-1h-5v-2z' transform='translate(0 1)' />
    </svg>".html_safe
  end

  def arrow_right(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M5 0v2h-5v1h5v2l3-2.53-3-2.47z' transform='translate(0 1)' />
    </svg>".html_safe
  end

  def play(options = {})
    "<svg xmlns='http://www.w3.org/2000/svg'
          class='#{options[:class]}'
          width='#{options[:width] || 8}'
          height='#{options[:height] || 8}'
          viewBox='0 0 8 8'>
      <path d='M0 0v6l6-3-6-3z' transform='translate(1 1)' />
    </svg>".html_safe
  end
end
