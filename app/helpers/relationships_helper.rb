module RelationshipsHelper
  def with_options
    relationships = Relationship.where(owner_id: @resource.owner_id)
    if @resource.persisted?
      relationships = relationships.where("id != ?", @resource.id)
    end
    related = relationships.pluck(:with_id)
    unrelated = Name.where(main: true)
                    .where(nameable_type: Person.name)
                    .where("nameable_id NOT IN (?)", [@resource.owner_id] + related)
                    .order(:value)
    unrelated.map do |name|
      [name.value, name.nameable_id]
    end
  end

  def relationship_degrees
    Relationship::DEGREES
  end

  def degree_eternal?(degree)
    Relationship::DEGREES_ETERNAL.include?(degree)
  end
end
