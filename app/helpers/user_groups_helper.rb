module UserGroupsHelper
  def users_options
    (User.all - @resource.users).map { |user| [user.email, user.id] }.sort_by(&:first)
  end
end
