module OrganizationMembersHelper
  def positions_options
    OrganizationPosition.all.map { |p| [display_name(p), p.id] }
  end

  def organization_departments_options
    return [] unless @resource.organization_id?
    @resource
      .organization
      .departments
      .includes(:location)
      .map do |department|
      [display_name(department), department.id, {
        :"data-location-id" => department.location_id,
        :"data-location-address" => display_name(department.location)
      }]
    end
  end
end
