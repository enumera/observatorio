namespace :observatorio do
  desc "Setup new Observatorio instance"
  task :setup => :environment do
    next if User.count.positive?
    puts "Creating Observatório admin user..."
    print "Enter admin e-mail address: "
    admin_email = STDIN.gets.strip
    admin_password = SecureRandom.urlsafe_base64
    admin = User.create!(email: admin_email, password: admin_password, confirmed_at: Time.now)
    puts
    puts "##---------------------------------------------##"
    puts "## Be sure to note down these credentials now! ##"
    puts "   -- email: #{admin_email}                    "
    puts "   -- password: #{admin_password}              "
    puts "##---------------------------------------------##"
    UserGroup.create!(name: I18n.t("observatorio.default_user_groups.administrators"),
                      users: [admin],
                      permissions: {
                        Person => ["create", "read", "update", "delete", "list"],
                        Organization => ["create", "read", "update", "delete", "list"],
                        EducationDegree => ["create", "read", "update", "delete", "list"],
                        EducationCourse => ["create", "read", "update", "delete", "list"],
                        Event => ["create", "read", "update", "delete", "list"],
                        EventRole => ["create", "read", "update", "delete", "list"],
                        Tag => ["create", "read", "update", "delete", "list"],
                        OrganizationPosition => ["create", "read", "update", "delete", "list"],
                        Location => ["create", "read"],
                        UserGroup => ["update", "read", "list"],
                        Import => ["create", "read", "list"]
                      })
    UserGroup.create!(name: I18n.t("observatorio.default_user_groups.members"),
                      permissions: {
                        Person => ["create", "read", "update", "delete", "list"],
                        Organization => ["create", "read", "update", "delete", "list"],
                        Event => ["create", "read", "update", "delete", "list"],
                        EventRole => ["read", "list"],
                        Tag => ["read", "list"],
                        EducationDegree => ["read", "list"],
                        EducationCourse => ["read", "list"],
                        OrganizationPosition => ["read", "list"],
                        Location => ["create", "read"],
                        Import => ["create", "read", "list"]
                      })
    UserGroup.create!(name: I18n.t("observatorio.default_user_groups.visitors"),
                      display_priority: 1,
                      permissions: {
                        Person => ["read", "list"],
                        Organization => ["read", "list"],
                        Event => ["read", "list"],
                        EventRole => ["read", "list"],
                        Tag => ["read", "list"],
                        EducationDegree => ["read", "list"],
                        EducationCourse => ["read", "list"],
                        OrganizationPosition => ["read", "list"],
                        Location => ["read"]
                      })
  end

  desc "Clear file storage"
  task :clear_files => :environment do
    Person.all.each do |person|
      person.picture.file.delete
    end
    Organization.all.each do |organization|
      organization.logo.file.delete
    end
    Import.all.each do |import|
      import.file.file.delete
    end
  end
end
