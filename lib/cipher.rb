class Cipher
  PROTOCOL = "AES-256-CBC" # https://en.wikipedia.org/wiki/Advanced_Encryption_Standard

  def initialize(key)
    @key = key
  end

  def encrypt(data, iv)
    cipher = OpenSSL::Cipher.new(PROTOCOL)
    cipher.encrypt
    cipher.iv = iv
    cipher.key = @key
    cipher.update(data) + cipher.final
  end

  def decrypt(encrypted, iv)
    cipher = OpenSSL::Cipher.new(PROTOCOL)
    cipher.decrypt
    cipher.iv = iv
    cipher.key = @key
    cipher.update(encrypted) + cipher.final
  end

  def random_iv
    SecureRandom.random_bytes(OpenSSL::Cipher.new(PROTOCOL).iv_len)
  end
end
