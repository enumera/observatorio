require 'paper_trail/frameworks/active_record/models/paper_trail/version'

module PaperTrail
  class Version < ActiveRecord::Base
    has_many :version_associations, class_name: "PaperTrail::VersionAssociation"

    # Gives the relevant item state on version
    #
    # - just after changes for 'create' events
    # - just before changes for other events
    def item_state(belongs_to: false)
      @item_state ||= {}
      @item_state[belongs_to] ||= begin
        if event == "create"
          if self.next.present?
            reified = self.next.reify(belongs_to: belongs_to)
            if belongs_to
              item_klass.reflect_on_all_associations(:belongs_to).each do |assoc|
                assoc_klass = assoc.polymorphic? ?
                  reified.send(assoc.foreign_type).constantize : assoc.klass
                next unless PaperTrail.request.enabled_for_model?(assoc_klass)
                record_id = reified.send(assoc.foreign_key)
                record = record_id.nil? ? nil : load_record(
                  record_id, assoc_klass, belongs_to
                )
                reified.send(:"#{assoc.name}=", record)
              end
            end
            reified
          else
            reloaded_item = item_klass.find(item_id)
            item_klass.reflect_on_all_associations(:belongs_to).each do |assoc|
              assoc_klass = assoc.polymorphic? ?
                reloaded_item.send(assoc.foreign_type).constantize : assoc.klass
              next unless PaperTrail.request.enabled_for_model?(assoc_klass)
              record_id = reloaded_item.send(assoc.foreign_key)
              record = record_id.nil? ? nil : load_record(
                record_id, assoc_klass, belongs_to
              )
              reloaded_item.send(:"#{assoc.name}=", record)
            end
            reloaded_item
          end
        else
          reified = reify(belongs_to: belongs_to)
          if belongs_to
            item_klass.reflect_on_all_associations(:belongs_to).each do |assoc|
              assoc_klass = assoc.polymorphic? ?
                reified.send(assoc.foreign_type).constantize : assoc.klass
              next unless PaperTrail.request.enabled_for_model?(assoc_klass)
              record_id = reified.send(assoc.foreign_key)
              record = record_id.nil? ? nil : load_record(
                record_id, assoc_klass, belongs_to
              )
              reified.send(:"#{assoc.name}=", record)
            end
          end
          reified
        end
      end
    end

    # Gives converted changeset values
    #
    # - dates & datetimes are parsed
    # - ids are replaced by records
    def converted_changeset
      @converted_changesets ||= begin
        changeset.inject({}) do |converted_h, (attribute, change)|
          converted_change = change.map do |value|
            next nil if value.nil?
            next nil if value.respond_to?(:empty?) && value.empty?
            next nil if attribute.to_sym == item_klass.primary_key.to_sym
            case item_klass.attribute_types[attribute].type
            when :uuid
              assoc = item_klass.reflect_on_all_associations(:belongs_to).detect do |a|
                a.foreign_key.to_sym == attribute.to_sym
              end
              assoc_klass = assoc.polymorphic? ?
                item_state.send(assoc.foreign_type).constantize : assoc.klass
              load_record(value, assoc_klass)
            when :date
              Date.parse(value)
            when :datetime
              DateTime.parse(value)
            else
              value
            end
          end
          converted_change.compact.empty? ?
            converted_h : converted_h.merge(attribute => converted_change)
        end
      end
    end

    def item_klass
      item_subtype.constantize
    end

    private

    def load_record(record_id, record_klass, belongs_to = false)
      loaded_records[[record_id, record_klass]] ||= begin
        if PaperTrail.request.enabled_for_model?(record_klass)
          version = PaperTrail::Version
                      .where(item_type: record_klass.name)
                      .where(item_id: record_id)
                      .where("created_at >= ? OR transaction_id = ?",
                              created_at, transaction_id)
                      .order(:id)
                      .limit(1)
                      .first
          if version.present?
            version.item_state(belongs_to: belongs_to)
          else
            reloaded_item = record_klass.find_by(id: record_id)
            if belongs_to
              record_klass.reflect_on_all_associations(:belongs_to).each do |assoc|
                assoc_klass = assoc.polymorphic? ?
                  reloaded_item.send(assoc.foreign_type).constantize : assoc.klass
                next unless PaperTrail.request.enabled_for_model?(assoc_klass)
                record_id = reloaded_item.send(assoc.foreign_key)
                record = record_id.nil? ? nil : load_record(
                  record_id, assoc_klass, false
                )
                reloaded_item.send(:"#{assoc.name}=", record)
              end
            end
            reloaded_item
          end
        else
          record_klass.find_by(id: record_id)
        end
      end
    end

    def loaded_records
      @loaded_records ||= {}
    end
  end
end
