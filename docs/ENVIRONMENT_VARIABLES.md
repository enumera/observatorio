# ENVIRONMENT VARIABLES TABLE

| Name | Description | |
| --- | --- | :---: |
| **AES_256_SECRET_KEY** | The **secret** encryption key ([Base64](https://en.wikipedia.org/wiki/Base64) encoded), generate it by running `bin/keygen` from root directory |
| **DATABASE_URL** | PostgreSQL's [connection URL](https://www.postgresql.org/docs/current/static/libpq-connect.html#id-1.7.3.8.3.6) | __*__ |
| **DEFAULT_TIME_ZONE** | Default users' time zone (see [the list of available time zones](https://framagit.org/lobster/observatorio/blob/feature/import/docs/AVAILABLE_TIME_ZONES.md)) |
| **CLOUDINARY_URL** | Copy the value from [your Cloudinary console](https://cloudinary.com/console) |
| **HOST** | The host name that will be used to create links in e-mail messages |
| **MAILER_SENDER** | The e-mail address used as sender of all outgoing e-mail messages |
| **MAPBOX_PUBLIC_TOKEN** | Copy the value from [your Mapbox access tokens page](https://www.mapbox.com/account/access-tokens) |
| **RAILS_ENV** | [Rails](https://rubyonrails.org/) application environment | __*__ |
| **RAILS_FORCE_SSL** | Set to any value (ex: `enabled`) to force all incoming requests to use [HTTPS protocol](https://en.wikipedia.org/wiki/HTTPS) |
| **RAILS_SECRET_KEY_BASE** | Rails uses this secret key to protect users' sessions (optionally use ` SECRET_KEY_BASE`) | __*__ |
| **RAILS_SERVE_STATIC_FILES** | Set to any value (ex: `enabled`) to make Observatório application to serve static files from its `public` folder | __*__ |
| **SMTP_ADDRESS** | SMTP server address
| **SMTP_PORT** | SMTP server port
| **SMTP_USERNAME** | E-mail account username
| **SMTP_PASSWORD** | E-mail account password
| **OPENSTACK_AUTH_VERSION** | OpenStack authentication version
| **OPENSTACK_AUTH_URL** | OpenStack authentication service URL
| **OPENSTACK_USERNAME** | OpenStack username
| **OPENSTACK_API_KEY** | OpenStack password
| **OPENSTACK_REGION** | OpenStack region
| **OPENSTACK_SWIFT_CONTAINER** | OpenStack Swift container

__*__ Automatically set by [Heroku](https://framagit.org/lobster/observatorio/blob/develop/docs/DEPLOY_HEROKU.md)

### Testing variables

| Name | Description |
| --- | --- |
| **LOCAL_STORAGE** | Set to any value (ex: `enabled`) to use local storage and skip OpenStack settings |
| **MAILER_DELIVERY_METHOD** | Set to `test` to disable e-mail delivery and skip SMTP settings |
