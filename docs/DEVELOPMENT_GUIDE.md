# DEVELOPMENT GUIDE

_This guide only works on [Unix-like operating systems](https://en.wikipedia.org/wiki/Unix-like) such as Linux or macOS_

#### Pre-requisites

- Install [Git](https://git-scm.com/downloads)
- Install [RVM](http://rvm.io/)
- Install [Docker](https://docs.docker.com/install/)
- Install [Docker Machine](https://docs.docker.com/machine/install-machine/)
- Install [Docker Compose](https://docs.docker.com/compose/install/)
- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- Install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html)
- Have a [Mapbox account](https://www.mapbox.com/signin/) (Mapbox's free of charge plan [allows 50,000 map views per month](https://www.mapbox.com/pricing/))
- Have a [Cloudinary account](https://cloudinary.com/users/login) (Cloudinary's free of charge plan [allows 300,000 images & videos](https://cloudinary.com/pricing))

#### Make sure wkhtmltopdf is working fine

```sh
wkhtmltopdf --version # must exit with status 0
```

#### Setup RVM

Add the following line to the bottom of your `.bashrc`, `.zshrc`, or `.tcshrz` file

```sh
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
```

...then **restart your terminal**

#### Install Ruby

```sh
rvm install ruby-2.6.5
```

#### Checkout source code

```sh
git clone https://framagit.org/lobster/observatorio.git && cd observatorio/
```

You may want to use a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

#### Install required libraries

```sh
gem install bundler && bundle install
```

#### Start development environment

This command creates a virtual machine running all free services

```sh
bin/develop start
```

#### Setup external services

[Download your customized cloudinary.yml](https://cloudinary.com/console/lui/cloudinary.yml) configuration file and place it under `config` directory.

Copy your [Mapbox public access token](https://www.mapbox.com/account/access-tokens) and set `MAPBOX_PUBLIC_TOKEN` environment variable.
```sh
export MAPBOX_PUBLIC_TOKEN=<mapbox-access-token>
```

#### Initialize database

This command initializes database schema & create admin account

```sh
rake db:create db:migrate db:test:prepare observatorio:setup
```

#### Run tests

```sh
rspec
```

#### Run Observatório in development mode

```sh
rails server
```

The application will be available at `http://localhost:3000`

#### Stopping development environment

This command stops the virtual machine running the development environment

```sh
bin/develop stop
```

## Removing everything

* Cancel your [Cloudinary account](https://cloudinary.com/console/settings/account)
* Delete your [Mapbox account](https://account.mapbox.com/settings/#profile)
* Remove development environment running `bin/develop remove` command
* Uninstall [wkhtmltopdf](https://wkhtmltopdf.org/)
* Uninstall [Docker Machine](https://docs.docker.com/machine/install-machine/#how-to-uninstall-docker-machine)
* Uninstall [Docker Compose](https://docs.docker.com/compose/install/#uninstallation)
* Uninstall [Docker](https://docs.docker.com/install/)
* Uninstall [VirtualBox](https://www.virtualbox.org/)
* Uninstall **RVM** running `rvm implode` command
