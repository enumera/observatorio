class AddCollationToCourses < ActiveRecord::Migration[5.0]
  def up
    change_column :courses, :name, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :courses, :name, :text
  end
end
