class FixCategoriesUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :categories, :name
    add_index    :categories, [:name, :type], unique: true
  end
end
