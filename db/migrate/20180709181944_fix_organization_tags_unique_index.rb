class FixOrganizationTagsUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :organization_tags, :name
    add_index :organization_tags, "lower(name) varchar_pattern_ops", unique: true
  end
end
