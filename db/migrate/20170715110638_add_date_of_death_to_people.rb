class AddDateOfDeathToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :date_of_death, :date
  end
end
