class CreateOrganizationPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_positions, id: :uuid do |t|
      t.timestamps                    null: false
      t.uuid       :organization_id,  null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
    end
    add_index :organization_positions, [:organization_id, :name], unique: true
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO organization_positions (created_at, updated_at, organization_id, name, display_priority)
            SELECT now(), now(), id, '#{I18n.t("observatorio.other")}', 1 FROM organizations
        SQL
      end
    end
  end
end
