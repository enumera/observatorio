class PopulateVersionsSubtypes < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      UPDATE versions SET item_subtype = item_type
        WHERE item_type != 'Tag';
      UPDATE versions SET item_subtype = tags.type
        FROM tags
        WHERE item_type = 'Tag' AND tags.id = item_id
    SQL
  end

  def down
    execute "UPDATE versions SET item_subtype = NULL"
  end
end
