class DeviseCreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, id: :uuid do |t|
      t.timestamps null: false
      ## Database authenticatable
      t.string :email,              null: false
      t.string :encrypted_password, null: false
    end
    add_index :users, :email,                unique: true
  end
end
