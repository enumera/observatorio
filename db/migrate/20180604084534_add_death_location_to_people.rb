class AddDeathLocationToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :death_location_id, :uuid
  end
end
