class AddIndexesToEventParticipations < ActiveRecord::Migration[5.0]
  def change
    add_index :event_participations, [:event_id]
    add_index :event_participations, [:person_id]
    add_index :event_participations, [:organization_id]
  end
end
