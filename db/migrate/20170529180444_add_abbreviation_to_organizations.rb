class AddAbbreviationToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :abbreviation, :text
  end
end
