class DropEvents < ActiveRecord::Migration[5.0]
  def change
    # Event categories
    remove_index :event_categories, column: :name, unique: true
    drop_table :event_categories, id: :uuid do |t|
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.text     "name",             null: false
      t.integer  "display_priority", null: false
    end
    # Event participations
    remove_index :event_participations, column: :event_id
    remove_index :event_participations, column: :organization_id
    remove_index :event_participations, column: :person_id
    drop_table :event_participations, id: :uuid do |t|
      t.uuid "event_id",        null: false
      t.uuid "person_id"
      t.uuid "organization_id"
      t.text "type"
      t.uuid "role_id",         null: false
    end
    # Event roles
    remove_index :event_roles, column: [:event_id, :name]
    drop_table :event_roles, id: :uuid do |t|
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.uuid     "event_id",         null: false
      t.text     "name",             null: false
      t.integer  "display_priority", null: false
    end
    # Events
    remove_index :events, column: [:date, :name]
    drop_table :events, id: :uuid do |t|
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.text     "name",        null: false
      t.date     "date",        null: false
      t.text     "description"
      t.uuid     "category_id", null: false
    end
    # Paper trail versions & Notes
    reversible do |dir|
      dir.up do
        execute "DELETE FROM versions WHERE item_type IN ('EventCategory', 'EventParticipation', 'EventRole', 'Event')"
        execute "DELETE FROM notes WHERE object_type IN ('EventCategory', 'EventParticipation', 'EventRole', 'Event')"
      end
    end
  end
end
