class RenameDepartmentsToOrganizationDepartments < ActiveRecord::Migration[5.0]
  def change
    rename_table :departments, :organization_departments
  end
end
