class AddSignatureToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :signature, :text
  end
end
