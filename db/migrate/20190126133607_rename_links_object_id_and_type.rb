class RenameLinksObjectIdAndType < ActiveRecord::Migration[5.2]
  def change
    rename_column :links, :object_id, :linkable_id
    rename_column :links, :object_type, :linkable_type
  end
end
