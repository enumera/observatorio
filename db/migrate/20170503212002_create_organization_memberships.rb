class CreateOrganizationMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_memberships, id: :uuid do |t|
      t.timestamps                   null: false
      t.uuid       :person_id,       null: false
      t.uuid       :organization_id, null: false
      t.text       :position,        null: true
      t.date       :since,           null: true
      t.date       :until,           null: true
    end
  end
end
