class AddHeadquartersToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :headquarters, :json
  end
end
