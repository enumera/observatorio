class RenameParticipationRoleToParticipationCategory < ActiveRecord::Migration[5.0]
  def change
    rename_table :participation_roles, :participation_categories
    rename_column :event_participations, :role_id, :category_id
  end
end
