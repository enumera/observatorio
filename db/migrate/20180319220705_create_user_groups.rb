class CreateUserGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :user_groups, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
      t.json       :permissions,      null: false
    end
    add_index :user_groups, [:name], unique: true
  end
end
