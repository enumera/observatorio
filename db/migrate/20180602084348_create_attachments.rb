class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments, id: :uuid do |t|
      t.timestamps                      null: false
      t.text       :label,              null: false
      t.uuid       :object_id,          null: false
      t.text       :object_type,        null: false
      t.text       :file,               null: false
      t.text       :file_extension,     null: false
      t.integer    :file_size,          null: false, limit: 8
      t.text       :file_encryption_iv, null: false
    end
    add_index :attachments, [:object_id, :object_type, :label], unique: true
  end
end
