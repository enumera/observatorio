class FixVersionsDeletedTables < ActiveRecord::Migration[5.1]
  def up
    execute "DELETE FROM versions WHERE item_type = 'OrganizationCategory'"
    execute "DELETE FROM versions WHERE item_type = 'Category'"
  end
end
