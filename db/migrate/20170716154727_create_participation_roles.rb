class CreateParticipationRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :participation_roles, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false, unique: true
      t.integer    :display_priority, null: false
    end
    add_column :event_participations, :role_id, :uuid
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO participation_roles (id, name, display_priority, created_at, updated_at)
            VALUES (uuid_generate_v4(), 'other', 1, now(), now());
          UPDATE event_participations
            SET role_id = participation_roles.id
            FROM participation_roles
            WHERE participation_roles.name = 'other';
        SQL
      end
    end
    change_column_null :event_participations, :role_id, false
  end
end
