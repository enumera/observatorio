class RefactorEventParticipation < ActiveRecord::Migration[5.0]
  def change
    add_column :event_participations, :person_id,       :uuid
    add_column :event_participations, :organization_id, :uuid
    add_column :event_participations, :type,            :text
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE event_participations
            SET person_id = participant_id,
                type = 'PersonParticipation'
            WHERE participant_type = 'Person';
          UPDATE event_participations
            SET organization_id = participant_id,
                type = 'OrganizationParticipation'
            WHERE participant_type = 'Organization';
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE event_participations
            SET participant_id = person_id,
                participant_type = 'Person'
            WHERE type = 'PersonParticipation';
          UPDATE event_participations
            SET participant_id = organization_id,
                participant_type = 'Organization'
            WHERE type = 'OrganizationParticipation';
        SQL
      end
    end
    change_column_null :event_participations, :type, false
    remove_column :event_participations, :participant_id,   :uuid
    remove_column :event_participations, :participant_type, :text
  end
end
