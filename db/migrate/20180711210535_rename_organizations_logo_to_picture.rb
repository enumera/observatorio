class RenameOrganizationsLogoToPicture < ActiveRecord::Migration[5.0]
  def change
    rename_column :organizations, :logo, :picture
  end
end
