class FixPositionsUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :positions, [:organization_id, :name]
    add_index :positions, "organization_id, lower(name) varchar_pattern_ops", unique: true, name: "index_to_positions_on_organization_and_name_case_insensitive"
  end
end
