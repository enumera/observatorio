class FromOrganizationCategoriesToOrganizationTags < ActiveRecord::Migration[5.0]
  def change
    rename_table :organization_categories, :organization_tags
    create_table :organizations_organization_tags, id: false do |t|
      t.timestamps null: false
      t.uuid :organization_id, null: false
      t.uuid :organization_tag_id, null: false
    end
    add_index :organizations_organization_tags, :organization_id
    add_index :organizations_organization_tags, :organization_tag_id
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO organizations_organization_tags
                      (created_at, updated_at, organization_id, organization_tag_id)
            SELECT now(), now(), id, category_id
            FROM organizations
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE organizations SET category_id = organizations_organization_tags.organization_tag_id
            FROM organizations_organization_tags
            WHERE organizations.id = organizations_organization_tags.organization_id
        SQL
      end
    end
    remove_column :organizations, :category_id, :uuid
  end
end
