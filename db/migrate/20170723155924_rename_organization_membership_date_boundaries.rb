class RenameOrganizationMembershipDateBoundaries < ActiveRecord::Migration[5.0]
  def change
    rename_column :organization_memberships, :since, :since_date
    rename_column :organization_memberships, :until, :until_date
  end
end
