class RenameMembershipsToOrganizationMembers < ActiveRecord::Migration[5.0]
  def change
    rename_table :memberships, :organizations_members
  end
end
