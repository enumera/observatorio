class AddDateBoundariesToOrganizationOwners < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations_owners, :from_date,         :date
    add_column :organizations_owners, :to_date,           :date
    add_column :organizations_owners, :approximate_dates, :boolean
  end
end
