class RemoveDeprecatedColumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :organizations,         :headquarters_old, :json
    remove_column :organizations_members, :location_old,     :json
  end
end
