class MigrateUuid < ActiveRecord::Migration[5.2]
  def up
    enable_extension "pgcrypto"
    %i(attachments education_courses education_degrees educations
       event_roles events events_participants events_tags imports
       links locations notes organization_departments organization_positions
       organizations organizations_members organizations_owners
       organizations_tags people people_tags relationships tags user_groups
       users).each do |table|
      change_column table, :id, :uuid, default: "gen_random_uuid()"
    end
    disable_extension "uuid-ossp"
  end

  def down
    enable_extension "uuid-ossp"
    %i(attachments education_courses education_degrees educations
       event_roles events events_participants events_tags imports
       links locations notes organization_departments organization_positions
       organizations organizations_members organizations_owners
       organizations_tags people people_tags relationships tags user_groups
       users).each do |table|
      change_column table, :id, :uuid, default: "uuid_generate_v4()"
    end
    disable_extension "pgcrypto"
  end
end
