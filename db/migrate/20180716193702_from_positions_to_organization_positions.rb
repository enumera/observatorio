class FromPositionsToOrganizationPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_positions, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
    end
    add_index :organization_positions, "lower(name) varchar_pattern_ops", unique: true
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO organization_positions (created_at, updated_at, name, display_priority)
            SELECT now(), now(), max(name), max(display_priority)
            FROM positions
            GROUP BY lower(name)
        SQL
        execute <<-SQL
          UPDATE memberships SET position_id = organization_positions.id
            FROM positions, organization_positions
            WHERE positions.id = memberships.position_id AND
                  lower(positions.name) = lower(organization_positions.name)
        SQL
      end
    end
    drop_table :positions, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
      t.uuid       :organization_id,  null: false
    end
  end
end
