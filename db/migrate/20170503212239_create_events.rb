class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events, id: :uuid do |t|
      t.timestamps        null: false
      t.text       :name, null: false
      t.date       :date, null: false
    end
    add_index :events, [:name, :date], unique: true
  end
end
