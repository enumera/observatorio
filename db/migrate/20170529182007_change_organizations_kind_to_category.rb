class ChangeOrganizationsKindToCategory < ActiveRecord::Migration[5.0]
  def change
    rename_column :organizations, :kind, :category
  end
end
