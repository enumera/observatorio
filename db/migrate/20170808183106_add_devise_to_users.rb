class AddDeviseToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      # Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      # Roles
      t.boolean :super_admin
      t.text    :role
    end
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE users SET confirmed_at = created_at,
                           super_admin = FALSE,
                           role = 'admin'
        SQL
      end
    end
    add_index :users, :confirmation_token,   unique: true
    change_column_null :users, :super_admin, false
  end
end
