class AddSchoolToOrganizationCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :organization_categories, :school, :boolean
    reversible do |dir|
      execute <<-SQL
        UPDATE organization_categories SET school = FALSE
      SQL
    end
    change_column_null :organization_categories, :school, false
  end
end
