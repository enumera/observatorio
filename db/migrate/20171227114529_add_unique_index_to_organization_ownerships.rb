class AddUniqueIndexToOrganizationOwnerships < ActiveRecord::Migration[5.0]
  def change
    add_index :organization_ownerships, [:owner_id, :owner_type, :organization_id], unique: true, name: "index_organization_ownerships_unique"
  end
end
