class AddParentIdToOrganizationDepartments < ActiveRecord::Migration[5.2]
  def change
    add_column :organization_departments, :parent_id, :uuid
    add_index  :organization_departments, :parent_id
  end
end
