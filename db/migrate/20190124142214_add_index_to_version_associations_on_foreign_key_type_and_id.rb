class AddIndexToVersionAssociationsOnForeignKeyTypeAndId < ActiveRecord::Migration[5.2]
  def change
    add_index :version_associations, [:foreign_type, :foreign_key_id]
  end
end
