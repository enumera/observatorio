class AddUniqueIndexToOrganizationsOnName < ActiveRecord::Migration[5.0]
  def change
    add_index :organizations, "lower(name) varchar_pattern_ops", unique: true
  end
end
