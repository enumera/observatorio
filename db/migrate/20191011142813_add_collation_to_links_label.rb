class AddCollationToLinksLabel < ActiveRecord::Migration[5.2]
  def up
    change_column :links, :label, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :links, :label, :text, collation: nil
  end
end
