class RenameOrganizationTagIdOnOrganizationsTags < ActiveRecord::Migration[5.1]
  def change
    rename_column :organizations_tags, :organization_tag_id, :tag_id
  end
end
