class RemoveImportsVersions < ActiveRecord::Migration[5.1]
  def up
    execute "DELETE FROM versions WHERE item_type = 'Import'"
  end
end
