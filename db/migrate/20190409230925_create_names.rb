class CreateNames < ActiveRecord::Migration[5.2]
  def change
    create_table :names, id: :uuid do |t|
      t.timestamps                 null: false
      t.boolean    :main,          null: false
      t.text       :value,         null: false
      t.uuid       :nameable_id,   null: false
      t.text       :nameable_type, null: false
    end
    add_index :names, "nameable_type, lower(value) varchar_pattern_ops",
      name: "index_names_on_value"
    add_index :names, [:nameable_type, :nameable_id, :main]
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO names (created_at, updated_at, main,
                             value, nameable_id, nameable_type)
            SELECT now(), now(), true,
                   first_name || ' ' || last_name, id, 'Person'
              FROM people;
          INSERT INTO names (created_at, updated_at, main,
                             value, nameable_id, nameable_type)
            SELECT now(), now(), true,
                   name, id, 'Organization'
              FROM organizations;
          INSERT INTO names (created_at, updated_at, main,
                             value, nameable_id, nameable_type)
            SELECT now(), now(), false,
                   abbreviation, id, 'Organization'
              FROM organizations
              WHERE length(abbreviation) > 0;
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE people
            SET first_name = substring(names.value from '\w+'),
                last_name = (regexp_match(names.value, '(\w*)\s(.*)'))[2]
            FROM names
              WHERE names.nameable_id = people.id AND
                    names.nameable_type = 'Person';
          UPDATE organizations
            SET name = names.value
            FROM names
              WHERE names.nameable_id = organizations.id AND
                    names.nameable_type = 'Organization' AND
                    names.main = TRUE;
          UPDATE organizations
            SET abbreviation = names.value
            FROM names
              WHERE names.nameable_id = organizations.id AND
                    names.nameable_type = 'Organization' AND
                    names.main = FALSE;
        SQL
      end
    end
    remove_column :people, :first_name, :text
    remove_column :people, :last_name, :text
    remove_column :organizations, :name, :text
    remove_column :organizations, :abbreviation, :text
  end
end
