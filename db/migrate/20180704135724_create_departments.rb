class CreateDepartments < ActiveRecord::Migration[5.0]
  def change
    create_table :departments, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false
      t.uuid       :organization_id,  null: false
      t.uuid       :location_id
      t.integer    :display_priority, null: false
    end
    add_index :departments, [:organization_id, :name], unique: true
  end
end
