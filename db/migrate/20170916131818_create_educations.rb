class CreateEducations < ActiveRecord::Migration[5.0]
  def change
    create_table :educations, id: :uuid do |t|
      t.timestamps                   null: false
      t.uuid       :person_id,       null: false
      t.uuid       :organization_id, null: false
      t.date       :since_date
      t.date       :until_date
    end
    add_index :educations, :person_id
    add_index :educations, :organization_id
  end
end
