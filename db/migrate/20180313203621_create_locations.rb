class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations, id: :uuid do |t|
      t.timestamps                null: false
      t.integer    :osm_id,       null: false, limit: 8
      t.text       :osm_type,     null: false
      t.float      :lat,          null: true
      t.float      :lng,          null: true
      t.json       :address,      null: false
      t.float      :boundingbox,  null: true,  array: true
      t.json       :geojson,      null: false
    end

    add_index      :locations, [:osm_id, :osm_type, :lat, :lng], unique: true
    add_index      :locations, [:lng, :lat], unique: true
  end
end
