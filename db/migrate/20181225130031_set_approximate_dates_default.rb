class SetApproximateDatesDefault < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE organizations SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE organizations_members SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE educations SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
          UPDATE people SET approximate_dates = FALSE
            WHERE approximate_dates IS NULL;
        SQL
      end
    end
    %i(organizations organizations_members educations people).each do |table|
      change_column_null table, :approximate_dates, false
    end
  end
end
