class CreateOrganizationOwnerships < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_ownerships, id: :uuid do |t|
      t.timestamps                   null: false
      t.uuid       :person_id,       null: false
      t.uuid       :organization_id, null: false
      t.float      :portion,         null: false
    end
    add_index :organization_ownerships, [:person_id, :organization_id], unique: true
  end
end
